<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>安装<?php echo C('INSTALL_PRODUCT_NAME');?>－<?php echo ($meta_title); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link rel="stylesheet" type="text/css" href="/Public/lyui/css/lyui.min.css">
    <script type="text/javascript" src="/Public/js/jquery-1.10.2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="/Public/lyui/js/lyui.min.js"></script>
    
</head>
<body style="background-color: #f6f6f6;">
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="sr-only">导航开关</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" target="_blank" href="/"><?php echo C('INSTALL_PRODUCT_NAME');?></a>
            </div>
            <div class="collapse navbar-collapse nav-collapse">
                <ul class="nav navbar-nav" id="step">
                    
    <li class="active"><a href="javascript:;">安装协议</a></li>
    <li class="active"><a href="javascript:;">环境检测</a></li>
    <li class="active"><a href="javascript:;">参数设置</a></li>
    <li class="active"><a href="javascript:;">开始安装</a></li>
    <li class="active"><a href="javascript:;">完成安装</a></li>

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo C('INSTALL_WEBSITE_DOMAIN');?>" target="_blank">官网</a></li>
                    <li><a href="<?php echo C('INSTALL_WEBSITE_DOMAIN');?>" target="_blank">授权</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            
    <div class="col-xs-12 col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span>恭喜您，<?php echo C('INSTALL_PRODUCT_NAME');?>安装完成！</span>
            </div>
            <div class="panel-body">
                <div class="alert alert-success text-center">
                    <h3>SUCCESS !</h3>
                </div>
                <div class="alert alert-danger margin-top">
                    后台管理员用户名adminroot 密码默认均为adminadmin，请尽快登录系统进行修改，防止系统被恶意攻击。
                </div>
                <a class="btn btn-primary" target="_blank" href="manage">登录后台</a>
                <a class="btn btn-success" target="_blank" href="index.html">访问首页</a>
            </div>
            <div class="panel-footer">
                <span>版权所有 (c) 2014－<?php echo date("Y",time()); echo C('INSTALL_COMPANY_NAME');?> 保留所有权利。</span>
            </div>
        </div>
    </div>

        </div>
    </div>
</body>
</html>