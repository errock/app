<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>安装<?php echo C('INSTALL_PRODUCT_NAME');?>－<?php echo ($meta_title); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link rel="stylesheet" type="text/css" href="/Public/lyui/css/lyui.min.css">
    <script type="text/javascript" src="/Public/js/jquery-1.10.2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="/Public/lyui/js/lyui.min.js"></script>
    
</head>
<body style="background-color: #f6f6f6;">
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="sr-only">导航开关</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" target="_blank" href="/"><?php echo C('INSTALL_PRODUCT_NAME');?></a>
            </div>
            <div class="collapse navbar-collapse nav-collapse">
                <ul class="nav navbar-nav" id="step">
                    
    <li class="active"><a href="javascript:;">安装协议</a></li>
    <li class="active"><a href="javascript:;">环境检测</a></li>
    <li class="active"><a href="javascript:;">参数设置</a></li>
    <li><a href="javascript:;">开始安装</a></li>
    <li><a href="javascript:;">完成安装</a></li>

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo C('INSTALL_WEBSITE_DOMAIN');?>" target="_blank">官网</a></li>
                    <li><a href="<?php echo C('INSTALL_WEBSITE_DOMAIN');?>" target="_blank">授权</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            
    <div class="col-xs-12 col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span>参数设置</span>
            </div>
            <div class="panel-body">
                <form class="form" action="/Index_step3.html" method="post" target="_self">
                    <h2>数据库连接信息</h2>
                    <div class="form-group">
                        <label class="control-label">数据库连接类型</label>
                        <div class="control-group">
                            <select name="db[DB_TYPE]" class="form-control">
                                <option>mysql</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">数据库服务器，数据库服务器IP，一般为127.0.0.1</label>
                        <div class="control-group">
                            <input class="form-control" type="text" name="db[DB_HOST]" value="127.0.0.1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">数据库名</label>
                        <div class="control-group">
                            <input class="form-control" type="text" name="db[DB_NAME]" value="<?php echo C('PRODUCT_MODEL');?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">是否覆盖同名数据库</label>
                        <div class="control-group">
                            <label class="radio-inline" for="cover0">
                                <input type="radio" id="cover0" class="radio" name="cover" value="0" checked="checked"> 不覆盖
                            </label>
                            <label class="radio-inline" for="cover1">
                                <input type="radio" id="cover1" class="radio" name="cover" value="1"> 覆盖
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">数据库用户名</label>
                        <div class="control-group">
                            <input class="form-control" type="text" name="db[DB_USER]" value="root">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">数据库密码</label>
                        <div class="control-group">
                            <input class="form-control" type="password" name="db[DB_PWD]" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">数据表前缀，同一个数据库运行多个系统时请修改为不同的前缀</label>
                        <div class="control-group">
                            <input class="form-control" type="text" name="db[DB_PREFIX]" value="<?php echo (C("ORIGINAL_TABLE_PREFIX")); ?>">
                        </div>
                    </div>
                </form>
                <input type="submit" class="btn btn-primary btn-block ajax-post" target-form="form" value="下一步">
                <a class="btn btn btn-default btn-block" href="<?php echo U('Install/Index/step2');?>">上一步</a>
            </div>
            <div class="panel-footer">
                <span>版权所有 (c) 2014－<?php echo date("Y",time()); echo C('INSTALL_COMPANY_NAME');?> 保留所有权利。</span>
            </div>
        </div>
    </div>

        </div>
    </div>
</body>
</html>