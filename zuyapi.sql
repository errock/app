/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : zuyapi

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-06-19 19:24:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pay_admin
-- ----------------------------
DROP TABLE IF EXISTS `pay_admin`;
CREATE TABLE `pay_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `username` varchar(50) NOT NULL COMMENT '后台用户名',
  `password` varchar(32) NOT NULL COMMENT '后台用户密码',
  `groupid` tinyint(1) unsigned DEFAULT '0' COMMENT '用户组',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `google_secret_key` varchar(128) NOT NULL DEFAULT '' COMMENT '谷歌令牌密钥',
  `mobile` varchar(255) NOT NULL DEFAULT '' COMMENT '手机号码',
  `session_random` varchar(50) NOT NULL DEFAULT '' COMMENT 'session随机字符串',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_admin
-- ----------------------------
INSERT INTO `pay_admin` VALUES ('1', 'adminroot', '81b5234976a55e5181f24d9eab8fb697', '1', '0', '', '15815295199', '9YSlb8OMbyh9lZove1eqIwizyqASgzMO');
INSERT INTO `pay_admin` VALUES ('2', 'test', '7aa5e695be95cdd64a88410a64dfe2c1', '2', '1528767314', '', '', 'NRHsC5f7kMWFz94QPTBLT6cxdyyvoUaS');

-- ----------------------------
-- Table structure for pay_apimoney
-- ----------------------------
DROP TABLE IF EXISTS `pay_apimoney`;
CREATE TABLE `pay_apimoney` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `payapiid` int(11) DEFAULT NULL,
  `money` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `freezemoney` decimal(15,3) NOT NULL DEFAULT '0.000' COMMENT '冻结金额',
  `status` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_apimoney
-- ----------------------------
INSERT INTO `pay_apimoney` VALUES ('10', '6', '207', '18000.0000', '0.000', '1');

-- ----------------------------
-- Table structure for pay_article
-- ----------------------------
DROP TABLE IF EXISTS `pay_article`;
CREATE TABLE `pay_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '分类ID',
  `groupid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '分组  0：所有 1：商户 2：代理',
  `title` varchar(300) NOT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1显示 0 不显示',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_article
-- ----------------------------
INSERT INTO `pay_article` VALUES ('1', '1', '0', '八月央行严打升级！大开罚单，环迅支付、汇付等机构躺枪', '&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 8月以来，人民银行对第三方支付机构的罚单不断。截止目前，多家支付机构违反支付业务规定，已有易联支付、汇付、环迅支付等近10多家支付机构被罚。就在今日公布消息，迅付信息科技有限公司，被中国人民银行上海分行发布“上海银罚字〔2017〕28号”处罚决定。罚款金额包括没收违法所得285696.89元及罚款150万元，罚款合计1785696.89元。这是截止今年，人行对第三方支付机构开出的一张最高金额大罚单了。同时，汇付、易联支付在内其它几家支付机构罚款明细如下：上海汇付数据服务有限公司因违反支付业务规定 罚款人民币6万元。汇潮支付有限公司违反支付业务规定被罚处以罚款人民币4万元；上海德颐网络技术有限公司违反支付业务规定被罚处以罚款人民币6万元；上海富友支付服务有限公司违反支付业务规定被罚处以罚款人民币3万元。上海电银信息技术有限公司、卡友支付服务有限公司因违反支付业务规定，均被罚限期改正，并各处6万元罚款。上海巾帼三六五企业服务有限公司、上海银联电子支付服务有限公司、上海乐易信息技术有限公司因为违反支付业务规定，分别被处以2万元、6万元、4万元罚款。易联支付有限公司因违反银行卡收单业务管理规定，被广州分行罚款601435.92元。', '1503808262', '8月以来，人民银行对第三方支付机构的罚单不断。截止目前，多家支付机构违反支付业务规定，已有易联支付、汇付、环迅支付等近10多家支付机构被罚。', '1', '1503812262');
INSERT INTO `pay_article` VALUES ('2', '1', '0', '华尔街上再掀投资热潮，支付与金融科技比翼双飞', '华尔街上赫赫有名的几大投行高盛、花旗、摩根大通，面对势不可挡的金融科技，再也无法淡定了。他们一起纷纷重金砸向了金融科技领域，在华尔街上跳起了一支支投资热舞。几组数据可以让你看到这场热舞的火辣与动感。&amp;nbsp;● 花旗、高盛和摩根大通是三大最活跃的投资者。花旗(包括花旗风险投资公司)参加了30轮投资，涉22家公司;高盛参与31轮投资，涉25家公司;摩根大通参与14轮投资，涉13家公司。（来自管理咨询公司Opimas的报告，按银行投资金融科技公司的数量排序）● 根据摩根大通透露，近期将投入96亿美元并聘用专业技术团队专攻大数据、机器人和云基础设施。&amp;nbsp;● “华尔街大银行及其他成熟的金融公司可能会在2017年通过44笔交易向金融科技领域投入创纪录的17亿美元”。（来自管理咨询公司Opimas的报告）● 自2012年以来，美国前十大银行(按管理资产)已向56家金融科技公司进行了72次总计36亿美元的投资。（来自CB Insights年度数据）重磅投资金融科技领域同时，高盛、摩根大通在企业内部更是深植科技基因。高盛CEO劳尔德•贝兰克梵(Lloyd Blankfein)最常挂在嘴边的一句话是：其实，我们是一家科技公司。高盛雇用的3.6万名员工中，9000名是程序员和技术工程师。&amp;nbsp;摩根大通于去年7月投入使用的一款金融合同解析软件COIN，通过机器学习和加密云网络技术，负责处理智能合约以及解析商业贷款协议，在几秒内就能将原先律师和贷款人员每年需要36万小时做完的工作完成，在大大降低错误率的同时保证全年无休。&amp;nbsp;乐在其中的不止投行大咖，麦肯锡、普华永道等世界著名的咨询公司也为这场热舞，演绎着优美的背景音乐。&amp;nbsp;麦肯锡咨询公司(McKinsey &amp;amp; Company)早在2015年便呼吁金融机构，紧抓金融科技发展机遇，否则将失去逾四成传统金融业务营收，失去超六成传统金融业务利润。&amp;nbsp;普华永道在4月份公布的报告中称，全球将近50%的金融服务公司计划在未来3到5年收购金融科技类创业公司。&amp;nbsp;当然金融科技的内涵极为丰富，包含支付、保险、规划、借贷/众筹、区块链、交易&amp;amp;投资、数据&amp;amp;分析以及安全在内八大主题。其中支付作为构建金融科技的基层设施，最受到青睐。&amp;nbsp;据某些记者了解，高盛从2012年至今，在支付领域共投资了6家公司，参加了八轮融资，总额约5.7亿美元。包括对新晋的越南移动钱包应用MoMo的B轮融资。&amp;nbsp;摩根大通也创新发布了自己在支付领域的新产品Chase Pay。并动用资源，收购MCX（美国最大的商户网络），同时与波士顿软件公司LevelUp展开深入合作，提升Chase Pay的含金量，壮大Chase Pay的竞争实力。&amp;nbsp;看着世界金融中心华尔街，上演着热情洋溢的金融科技投资热舞。回看我们大洋彼岸的中国，中国金融科技市场更是一片欣欣向荣。从2016年全球与中国金融科技投融资情况统计图表，可以看出。', '1503808362', '华尔街上赫赫有名的几大投行高盛、花旗、摩根大通，面对势不可挡的金融科技，再也无法淡定了。他们一起纷纷重金砸向了金融科技领域，在华尔街上跳起了一支支投资热舞。几组数据可以让你看到这场热舞的火辣与动感。', '1', '0');
INSERT INTO `pay_article` VALUES ('3', '1', '0', '爆料：12家支付机构吃了工商税务的罚单 最高被罚8万元', '以下为中国支付网根据“国家企业信用信息公示系统”的数据进行统计，部分信息来自于“天眼查”。本次统计截至日期为2017年8月22日，统计范围为当前持有《支付业务许可证》的247家支付机构。&amp;nbsp;这12家机构分别是通联支付网络服务股份有限公司、上海银联电子支付服务有限公司、天津城市一卡通有限公司、厦门易通卡运营有限责任公司、安易联融电子商务有限公司、广西支付通商务服务有限公司、海南海岛一卡通支付网络有限公司、广东汇卡商务服务有限公司、供销中百支付有限公司、银盈通支付有限公司、北京国华汇银科技有限公司。', '1503808474', '《企业信息公示暂行条例》规定，有关部门应当建立健全信用约束机制，在政府采购、工程招投标、国有土地出让、授予荣誉称号等工作中，将企业信息作为重要考量因素，对被列入经营异常名录或者严重违法企业名单的企业依法予以限制或者禁入。', '1', '0');
INSERT INTO `pay_article` VALUES ('4', '1', '0', '银联ChinaPay、巾帼三六五、乐易三家支付机构被上海人行处罚', '上海巾帼三六五企业服务有限公司由上海巾帼社会服务有限公司组建，是一支隶属于上海市妇联的社会专业团队。2017年6月26日，央行正式公布第四批支付牌照续展名单，上海巾帼三六五企业服务有限公司成功续展《支付业务许可证》，支付业务类型包括预付卡发行与受理。► 中国人民银行上海分行于2017年8月15日作出行政处罚决定，上海银联电子支付服务有限公司因“违反支付业务规定”，被罚款6万元，行政处罚决定书文号是上海银罚〔2017〕25号。据中国支付网统计，这是上海银联首次被公开处罚。&amp;nbsp;上海银联电子支付服务有限公司(ChinaPay)是由中国银联控股，支付业务类型包括互联网支付、移动电话支付。&quot;银联在线&quot;是中国银联倾力打造的互联网业务综合商务门户网站，依托具有中国自主知识产权、国内领先的银联CUPSecure互联网安全认证支付系统和银联EBPP互联网收单系统，构建了银联便民支付网上平台、银联理财平台、银联网上商城三大业务平台，为广大持卡人提供公共事业缴费、通信缴费充值、信用卡还款、跨行转账、账单号支付、机票预订、基金理财和商城购物等全方位的互联网金融支付服务。► 中国人民银行上海分行于2017年8月15日作出行政处罚决定，上海乐易信息技术有限公司因“违反支付业务规定”，被罚款4万元，行政处罚决定书文号是上海银罚〔2017〕26号。据中国支付网统计，这是上海乐易首次被公开处罚。&amp;nbsp;上海乐易信息技术有限公司是容大集团控股的从事第三方支付和电子商务的专业化服务公司。2017年6月26日，央行正式公布第四批支付牌照续展名单，上海乐易成功续展《支付业务许可证》，支付业务类型包括预付卡发行与受理。', '1503808535', '中国人民银行上海分行于2017年8月15日作出行政处罚决定，上海巾帼三六五企业服务有限公司因“违反支付业务规定”，被罚款2万元，行政处罚决定书文号是上海银罚〔2017〕24号。据中国支付网统计，这是上海巾帼首次被公开处罚。', '1', '1503809486');
INSERT INTO `pay_article` VALUES ('5', '1', '0', '支付宝被列入“经营异常名录”！ 从此，支付宝被刻上“不良记录”烙印', '8月16日，网上曝出支付宝（中国）信息技术有限公司被监管部门列入 “经营异常名录”。什么？没搞错吧？小编马上到国家企业信用信息公示系统上查询求证，是真的！支付宝确实上了“经营异常名录”。有图有真相。&amp;nbsp;&amp;nbsp;&amp;nbsp;图中清晰看到，支付宝因未依照《企业信息公示暂行条例》第八条规定的限期公示年度报告，2017年7月7日，由中国（上海）自由贸易试验区市场监督管理局将其列入经营异常名录。&amp;nbsp;特意翻阅了下《企业信息公示暂行条例》第八条规定：也就是说，支付宝迟迟未上报自己企业的年度报告，从而被列入了“异常经营名录”。是支付宝太忙了，忘了上报？还是支付宝企业有什么不可告人的秘密、藏有猫腻，那就不得而知了！不管是何种原因，但这个结果，就像是在支付宝的信任大楼里砸出了一道裂缝。对支付宝的信任感降级了！众所周知，“企业信用”对一家企业来说是项非常重要的无形资产，特别是像支付宝这样的大型支付企业，它的信用值好坏牵动着中国上亿用户的资金安全，更直接影响着广大中小型企业与支付宝的合作发展。国家企业信用信息公示系统自2014年上线以来，以一个社会中介机构的角色，客观、公正的对各企业评定资信情况，取得良好信用等级的企业，能吸引广大企业放心大胆的与之合作；相反，如果信用等级不佳或有问题，就犹如在双方合作面前出现了拦路虎，阻挡了合作的可能。信用，是一个企业发展之根本。企业一旦被载入经营异常名录，这个“不良记录”的烙印就将与企业并存。也就是说，即使之后支付宝被移出了经营异常名录，但曾经被载入不良名单的记录并不会消失，这不但影响着企业声誉，就连企业日常经营也会受限，例如：企业在需要申请办理各类登记备案事项、行政许可审批事项和资质审核等，都会因为这个曾有的“不良记录”遭遇行政管理部门审慎审查，被限制或者禁入。&amp;nbsp;支付宝树大招风，坏消息也不止这一个。前不久，据说，蚂蚁金服被央行约谈不得用“无现金”宣传，上周末，余额宝宣布限额10万，年化利率下跌4%以下；这周，支付宝“亲密付”出现网络诈骗，有人被骗上万元….. 坏消息接踵而至，支付宝再次被推到风口浪尖上，支付宝是见过大场面的，相信也是没有在怕的。但这次被监管部门载入经营异常名录，无凝是对支付宝永久的伤害。当初以解决人与人之间信任为初衷而成立的支付宝，如今支付宝的企业信任值却出现瑕疵。这不免让人觉得有点讽刺。看来，支付宝，这次是真的失策了。马云爸爸或许马上就要来霸屏了。', '1503808590', '8月16日，网上曝出支付宝（中国）信息技术有限公司被监管部门列入 “经营异常名录”。什么？没搞错吧？', '1', '0');
INSERT INTO `pay_article` VALUES ('11', '4', '0', '银联通道维护！', '银联通道维护！', '1503810697', '银联通道维护！', '1', '0');
INSERT INTO `pay_article` VALUES ('12', '4', '0', '支付宝扫码通道维护！', '支付宝扫码通道维护！', '1503810718', '支付宝扫码通道维护！', '1', '1527661258');
INSERT INTO `pay_article` VALUES ('13', '1', '0', '文章1', '1', '0', '1', '1', '0');
INSERT INTO `pay_article` VALUES ('14', '1', '1', 'test', '1', '1527750819', '11', '1', '1527750832');
INSERT INTO `pay_article` VALUES ('15', '1', '2', 'test2', '11', '1527004800', '1111', '1', '0');

-- ----------------------------
-- Table structure for pay_attachment
-- ----------------------------
DROP TABLE IF EXISTS `pay_attachment`;
CREATE TABLE `pay_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户编号',
  `filename` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_attachment
-- ----------------------------
INSERT INTO `pay_attachment` VALUES ('48', '2', '242dd42a2834349b88359f1eccea15ce36d3be7e.jpg', 'Uploads/verifyinfo/59a2b65d0816c.jpg');
INSERT INTO `pay_attachment` VALUES ('46', '2', '6-140F316125V44.jpg', 'Uploads/verifyinfo/59a2b65cd9877.jpg');
INSERT INTO `pay_attachment` VALUES ('47', '2', '6-140F316132J02.jpg', 'Uploads/verifyinfo/59a2b65cea2ec.jpg');

-- ----------------------------
-- Table structure for pay_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `pay_auth_group`;
CREATE TABLE `pay_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_manager` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1需要验证权限 0 不需要验证权限',
  `rules` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_auth_group
-- ----------------------------
INSERT INTO `pay_auth_group` VALUES ('1', '超级管理员', '1', '0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100');
INSERT INTO `pay_auth_group` VALUES ('2', '运营管理员', '1', '0', '1,49,3,4,5,6,43,44,7,12,13,14,15,18,19,22,23,24,46,33,34,35,36,37,38,39,40,41,42,45,47,50,51,52,54,55');
INSERT INTO `pay_auth_group` VALUES ('3', '财务管理员', '1', '1', '1,77,5,23,65,66,24,67,6,13,68,69,70,71,73,76,25,72,26,74,75');
INSERT INTO `pay_auth_group` VALUES ('4', '普通商户', '1', '1', '');
INSERT INTO `pay_auth_group` VALUES ('5', '普通代理商', '1', '1', '');

-- ----------------------------
-- Table structure for pay_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `pay_auth_group_access`;
CREATE TABLE `pay_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_auth_group_access
-- ----------------------------
INSERT INTO `pay_auth_group_access` VALUES ('1', '1');
INSERT INTO `pay_auth_group_access` VALUES ('2', '1');
INSERT INTO `pay_auth_group_access` VALUES ('2', '2');
INSERT INTO `pay_auth_group_access` VALUES ('7', '2');

-- ----------------------------
-- Table structure for pay_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `pay_auth_rule`;
CREATE TABLE `pay_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(100) DEFAULT '' COMMENT '图标',
  `menu_name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则唯一标识Controller/action',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `pid` tinyint(5) NOT NULL DEFAULT '0' COMMENT '菜单ID ',
  `is_menu` tinyint(1) unsigned DEFAULT '0' COMMENT '1:是主菜单 0否',
  `is_race_menu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1:是 0:不是',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pay_auth_rule
-- ----------------------------
INSERT INTO `pay_auth_rule` VALUES ('1', 'fa fa-home', 'Index/index', '管理首页', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('2', 'fa fa-cogs', 'System/#', '系统设置', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('3', 'fa fa-cog', 'System/base', '基本设置', '2', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('4', 'fa fa-envelope-o', 'System/email', '邮件设置', '2', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('5', 'fa fa-send', 'System/smssz', '短信设置', '2', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('6', 'fa fa-hourglass', 'System/planning', '计划任务', '2', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('7', 'fa fa-user-circle', 'Admin/#', '管理员管理', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('8', 'fa fa-vcard ', 'Admin/index', '管理员信息', '7', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('9', 'fa fa-street-view', 'Auth/index', '角色配置', '7', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('10', 'fa fa-universal-access', 'Menu/index', '权限配置', '7', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('11', 'fa fa-users', 'User/#', '用户管理', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('12', 'fa fa-user', 'User/index?status=1&authorized=1', '已认证用户', '11', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('13', 'fa fa-user-o', 'User/index?status=1&authorized=2', '待认证用户', '11', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('14', 'fa fa-user-plus', 'User/index?status=1&authorized=0', '未认证用户', '11', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('15', 'fa fa-user-times', 'User/index?status=0', '冻结用户', '11', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('16', 'fa fa-gift', 'User/invitecode', '邀请码', '11', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('17', 'fa fa-address-book', 'User/loginrecord', '登录记录', '11', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('18', 'fa fa-handshake-o', 'Agent/#', '代理管理', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('19', 'fa fa-hand-lizard-o', 'User/agentList', '代理列表', '18', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('20', 'fa fa-signing', 'Order/changeRecord?bank=9', '佣金记录', '18', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('21', 'fa fa-sellsy', 'Order/dfApiOrderList', '代付Api订单', '18', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('22', 'fa fa-reorder', 'User/#', '订单管理', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('23', 'fa fa-indent', 'Order/changeRecord', '资金流水', '22', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('24', 'fa fa-thumbs-up', 'Order/index?status=1or2', '成功订单', '22', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('25', 'fa fa-thumbs-down', 'Order/index?status=0', '未支付订单', '22', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('26', 'fa fa-hand-o-right', 'Order/index?status=1', '异常订单', '22', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('27', 'fa fa-user-secret', 'Withdrawal', '提款管理', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('28', 'fa fa-wrench', 'Withdrawal/setting', '提款设置', '27', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('29', 'fa fa-asl-interpreting', 'Withdrawal/index', '手动结算', '27', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('30', 'fa fa-window-restore', 'Withdrawal/payment', '代付结算', '27', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('31', 'fa fa-bank', 'Channel/#', '通道管理', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('32', 'fa fa-product-hunt', 'Channel/index', '入金渠道设置', '31', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('33', 'fa fa-sitemap', 'Channel/product', '支付产品设置', '31', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('34', 'fa fa-sliders', 'PayForAnother/index', '代付渠道设置', '31', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('35', 'fa fa-book', 'Content/#', '文章管理', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('36', 'fa fa-tags', 'Content/category', '栏目列表', '35', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('37', 'fa fa-list-alt', 'Content/article', '文章列表', '35', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('38', 'fa fa-line-chart', 'Statistics/#', '财务分析', '0', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('39', 'fa fa-bar-chart-o', 'Statistics/index', '交易统计', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('40', 'fa fa-area-chart', 'Statistics/userFinance', '商户交易统计', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('41', 'fa fa-industry', 'Statistics/userFinance?groupid=agent', '代理商交易统计', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('42', 'fa fa-pie-chart', 'Statistics/channelFinance', '接口交易统计', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('43', 'fa fa-cubes', 'Template/index', '模板设置', '2', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('44', 'fa fa-mobile', 'System/mobile', '手机设置', '2', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('45', 'fa fa-signal', 'Statistics/chargeRank', '充值排行榜', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('46', 'fa fa-first-order', 'Deposit/index', '投诉保证金设置', '22', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('47', 'fa fa-asterisk', 'Statistics/complaintsDeposit', '投诉保证金统计', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('48', 'fa fa-database', 'System/clearData', '数据清理', '2', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('49', '', 'Index/main', 'Dashboard', '1', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('50', 'fa fa-area-chart', 'Statistics/platformReport', '平台报表', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('51', 'fa fa-area-chart', 'Statistics/merchantReport', '商户报表', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('52', 'fa fa-area-chart', 'Statistics/agentReport', '代理报表', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('53', 'fa fa-area-chart', 'Statistics/frozenMoney', '冻结资金统计', '38', '1', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('54', '', 'Payment/index', '提交代付', '0', '0', '0', '1', '1', '');
INSERT INTO `pay_auth_rule` VALUES ('55', '', 'Withdrawal/submitDf', '提交代付', '0', '0', '0', '1', '1', '');

-- ----------------------------
-- Table structure for pay_auto_df_log
-- ----------------------------
DROP TABLE IF EXISTS `pay_auto_df_log`;
CREATE TABLE `pay_auto_df_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `df_id` int(11) NOT NULL DEFAULT '0' COMMENT '代付ID',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型：1提交 2查询',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '结果 0：提交失败 1：提交成功 2：代付成功 3：代付失败',
  `msg` varchar(255) DEFAULT '' COMMENT '描述',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '提交时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_auto_df_log
-- ----------------------------

-- ----------------------------
-- Table structure for pay_auto_unfrozen_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_auto_unfrozen_order`;
CREATE TABLE `pay_auto_unfrozen_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `freeze_money` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '冻结金额',
  `unfreeze_time` int(11) NOT NULL DEFAULT '0' COMMENT '计划解冻时间',
  `real_unfreeze_time` int(11) NOT NULL DEFAULT '0' COMMENT '实际解冻时间',
  `is_pause` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否暂停解冻 0正常解冻 1暂停解冻',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '解冻状态 0未解冻 1已解冻',
  `create_at` int(11) NOT NULL COMMENT '记录创建时间',
  `update_at` int(11) NOT NULL COMMENT '记录更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_unfreezeing` (`status`,`is_pause`,`unfreeze_time`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='投诉保证金余额';

-- ----------------------------
-- Records of pay_auto_unfrozen_order
-- ----------------------------
INSERT INTO `pay_auto_unfrozen_order` VALUES ('1', '2', '1.0000', '1528422720', '1528428990', '0', '1', '1528422302', '1528428990');
INSERT INTO `pay_auto_unfrozen_order` VALUES ('2', '2', '1.0000', '1528646400', '1528525926', '1', '1', '1528524518', '1528524518');
INSERT INTO `pay_auto_unfrozen_order` VALUES ('3', '2', '2.0000', '1528527554', '1528527687', '0', '1', '1528527557', '1528527557');
INSERT INTO `pay_auto_unfrozen_order` VALUES ('4', '2', '1.0000', '1528700581', '1528527896', '0', '1', '1528527789', '1528527789');
INSERT INTO `pay_auto_unfrozen_order` VALUES ('5', '2', '2.0000', '0', '1528527918', '0', '1', '1528527908', '1528527908');
INSERT INTO `pay_auto_unfrozen_order` VALUES ('6', '2', '3.0000', '0', '1528528030', '0', '1', '1528528019', '1528528019');
INSERT INTO `pay_auto_unfrozen_order` VALUES ('7', '2', '11.0000', '1530288000', '0', '0', '0', '1528528180', '1528528180');

-- ----------------------------
-- Table structure for pay_bankcard
-- ----------------------------
DROP TABLE IF EXISTS `pay_bankcard`;
CREATE TABLE `pay_bankcard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户编号',
  `bankname` varchar(100) NOT NULL COMMENT '银行名称',
  `subbranch` varchar(100) NOT NULL COMMENT '支行名称',
  `accountname` varchar(100) NOT NULL COMMENT '开户名',
  `cardnumber` varchar(100) NOT NULL COMMENT '银行卡号',
  `province` varchar(100) NOT NULL COMMENT '所属省',
  `city` varchar(100) NOT NULL COMMENT '所属市',
  `ip` varchar(100) DEFAULT NULL COMMENT '上次修改IP',
  `ipaddress` varchar(300) DEFAULT NULL COMMENT 'IP地址',
  `alias` varchar(255) DEFAULT '' COMMENT '备注',
  `isdefault` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认 1是 0 否',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `IND_UID` (`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_bankcard
-- ----------------------------
INSERT INTO `pay_bankcard` VALUES ('44', '2', '中国工商银行', '广州石牌支行', '马培坚', '6212263602033031881', '广东省', '广州市', null, null, '工行卡', '0', '0');

-- ----------------------------
-- Table structure for pay_blockedlog
-- ----------------------------
DROP TABLE IF EXISTS `pay_blockedlog`;
CREATE TABLE `pay_blockedlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(100) NOT NULL COMMENT '订单号',
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户编号',
  `amount` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '冻结金额',
  `thawtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '解冻时间',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '商户支付通道',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1 解冻 0 冻结',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='资金冻结待解冻记录';

-- ----------------------------
-- Records of pay_blockedlog
-- ----------------------------
INSERT INTO `pay_blockedlog` VALUES ('1', '20180604140857575297', '2', '0.0100', '1528303931', '903', '1528258643', '0');
INSERT INTO `pay_blockedlog` VALUES ('2', '20180607131100524956', '2', '0.9900', '1528392869', '907', '1528354899', '0');
INSERT INTO `pay_blockedlog` VALUES ('3', '20180615191459515352', '2', '0.9920', '1529425054', '907', '1529391569', '0');

-- ----------------------------
-- Table structure for pay_browserecord
-- ----------------------------
DROP TABLE IF EXISTS `pay_browserecord`;
CREATE TABLE `pay_browserecord` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `articleid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_browserecord
-- ----------------------------

-- ----------------------------
-- Table structure for pay_category
-- ----------------------------
DROP TABLE IF EXISTS `pay_category`;
CREATE TABLE `pay_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '父级ID',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='文章栏目';

-- ----------------------------
-- Records of pay_category
-- ----------------------------
INSERT INTO `pay_category` VALUES ('1', '最新资讯', '0', '1');
INSERT INTO `pay_category` VALUES ('2', '公司新闻', '0', '1');
INSERT INTO `pay_category` VALUES ('3', '公告通知', '0', '1');
INSERT INTO `pay_category` VALUES ('4', '站内公告', '3', '1');
INSERT INTO `pay_category` VALUES ('5', '公司新闻', '3', '1');

-- ----------------------------
-- Table structure for pay_channel
-- ----------------------------
DROP TABLE IF EXISTS `pay_channel`;
CREATE TABLE `pay_channel` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '供应商通道ID',
  `code` varchar(200) DEFAULT NULL COMMENT '供应商通道英文编码',
  `title` varchar(200) DEFAULT NULL COMMENT '供应商通道名称',
  `mch_id` varchar(100) DEFAULT NULL COMMENT '商户号',
  `signkey` varchar(500) DEFAULT NULL COMMENT '签文密钥',
  `appid` varchar(100) DEFAULT NULL COMMENT '应用APPID',
  `appsecret` varchar(100) DEFAULT NULL COMMENT '安全密钥',
  `gateway` varchar(300) DEFAULT NULL COMMENT '网关地址',
  `pagereturn` varchar(255) DEFAULT NULL COMMENT '页面跳转网址',
  `serverreturn` varchar(255) DEFAULT NULL COMMENT '服务器通知网址',
  `defaultrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '下家费率',
  `fengding` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '封顶手续费',
  `rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '银行费率',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次更改时间',
  `unlockdomain` varchar(100) NOT NULL COMMENT '防封域名',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1开启 0关闭',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '渠道类型: 1 微信扫码 2 微信H5 3 支付宝扫码 4 支付宝H5 5网银跳转 6网银直连 7百度钱包 8 QQ钱包 9 京东钱包',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `paying_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '当天交易金额',
  `all_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '当天上游可交易量',
  `last_paying_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后交易时间',
  `min_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔最小交易额',
  `max_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔最大交易额',
  `control_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '风控状态:0否1是',
  `offline_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '通道上线状态:0已下线，1上线',
  `t0defaultrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0运营费率',
  `t0fengding` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0封顶手续费',
  `t0rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0成本费率',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=215 DEFAULT CHARSET=utf8 COMMENT='供应商列表';

-- ----------------------------
-- Records of pay_channel
-- ----------------------------
INSERT INTO `pay_channel` VALUES ('199', 'WxSm', '微信扫码支付', '', '', '', '', '', '', '', '0.0400', '0.0900', '0.0000', '1503846107', '', '1', '1', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0400', '0.0900', '0.0000');
INSERT INTO `pay_channel` VALUES ('200', 'WxGzh', '微信H5', '', '', 'wxf33668d58442ff6e', '', '', '', '', '0.0000', '0.0000', '0.0000', '1502378687', '', '1', '2', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('201', 'Aliscan', '支付宝扫码', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1503857975', '', '1', '3', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('202', 'Aliwap', '支付宝H5', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1503857966', '', '1', '4', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('203', 'QQSCAN', 'QQ扫码', '', '', '', '', '', '', '', '0.0050', '0.0000', '0.0000', '1503280494', '', '1', '8', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0050', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('207', 'ceishi', '测试支付', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1516441965', '', '1', '1', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('208', 'AliwapOld', '支付宝WAP（老版）', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1527738054', '', '1', '4', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('209', 'LzfBank', '乐支付网银', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1527755052', '', '1', '5', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('210', 'CodepayAliSm', '码支付（支付宝扫码）', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1528083079', '', '1', '3', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('211', 'UnionpayWgMobile', '银联网关支付', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1528340853', 'http://payvip.subangjinlong.com', '1', '5', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('212', 'Rzfwxscan', '睿支付微信扫码', '', '', '', '', '', '', '', '0.0050', '0.0080', '0.0030', '1529047760', '', '1', '1', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0050', '0.0080', '0.0031');
INSERT INTO `pay_channel` VALUES ('213', 'Htbank', '汇通网关支付', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1529048021', '', '1', '5', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel` VALUES ('214', 'Rzfbank', '睿支付网银支付', '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', '1529057456', '', '1', '5', '0', '0', '0.00', '0.00', '0', '0.00', '0.00', '0', '1', '0.0000', '0.0000', '0.0000');

-- ----------------------------
-- Table structure for pay_channel_account
-- ----------------------------
DROP TABLE IF EXISTS `pay_channel_account`;
CREATE TABLE `pay_channel_account` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '供应商通道账号ID',
  `channel_id` smallint(6) unsigned NOT NULL COMMENT '通道id',
  `mch_id` varchar(100) DEFAULT NULL COMMENT '商户号',
  `signkey` varchar(500) DEFAULT NULL COMMENT '签文密钥',
  `appid` varchar(100) DEFAULT NULL COMMENT '应用APPID',
  `appsecret` varchar(2500) DEFAULT NULL COMMENT '安全密钥',
  `defaultrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '下家费率',
  `fengding` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '封顶手续费',
  `rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '银行费率',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次更改时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1开启 0关闭',
  `title` varchar(100) DEFAULT NULL COMMENT '账户标题',
  `weight` tinyint(2) DEFAULT NULL COMMENT '轮询权重',
  `custom_rate` tinyint(1) DEFAULT NULL COMMENT '是否自定义费率',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始交易时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `last_paying_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后一笔交易时间',
  `paying_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '当天交易金额',
  `all_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单日可交易金额',
  `max_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔交易最大金额',
  `min_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔交易最小金额',
  `offline_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '上线状态-1上线,0下线',
  `control_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '风控状态-0不风控,1风控中',
  `is_defined` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否自定义:1-是,0-否',
  `unit_frist_paying_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间第一笔交易时间',
  `unit_paying_number` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '单时间交易笔数',
  `unit_paying_amount` decimal(11,0) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间交易金额',
  `unit_interval` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间数值',
  `time_unit` char(1) NOT NULL DEFAULT 's' COMMENT '限制时间单位',
  `unit_number` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间次数',
  `unit_all_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单位时间金额',
  `t0defaultrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0运营费率',
  `t0fengding` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0封顶手续费',
  `t0rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0成本费率',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=231 DEFAULT CHARSET=utf8 COMMENT='供应商账号列表';

-- ----------------------------
-- Records of pay_channel_account
-- ----------------------------
INSERT INTO `pay_channel_account` VALUES ('218', '199', '', '', '', '', '0.0400', '0.0900', '0.0000', '1513408073', '1', '微信扫码支付', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0400', '0.0900', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('219', '200', '', '', 'wxf33668d58442ff6e', '', '0.0000', '0.0000', '0.0000', '1513408073', '1', '微信H5', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('220', '201', '', '', '', '', '0.0000', '0.0000', '0.0000', '1513408073', '1', '支付宝扫码', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('221', '202', '', '', '', '', '0.0000', '0.0000', '0.0000', '1513408073', '1', '支付宝H5', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('222', '203', '', '', '', '', '0.0050', '0.0000', '0.0000', '1513408073', '1', 'QQ扫码', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0050', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('223', '207', '测试商户号', '测试证书密钥', '', '', '0.0000', '0.0000', '0.0000', '1516441979', '1', '测试名称', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('224', '208', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '', '0.0000', '0.0000', '0.0000', '1527738739', '1', '2088131085175285', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('225', '209', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '', '0.0000', '0.0000', '0.0000', '1527751621', '1', '10003', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('226', '210', '15575', 'm9VdgCnAZynwxK5sTTH2dXSyXmEAPBei', '', '', '0.0000', '0.0000', '0.0000', '1528082947', '1', '15575', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('227', '211', '827320557320008', '', '', '', '0.0000', '0.0000', '0.0000', '1528283218', '1', '827320557320008', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('228', '212', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '', '0.0060', '0.0080', '0.0030', '1529047649', '1', '100000057', '1', '1', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0061', '0.0080', '0.0030');
INSERT INTO `pay_channel_account` VALUES ('229', '213', '2', '893701E6A0C5EB903BFF6A12818ED9E0', '', 'test123456', '0.0000', '0.0000', '0.0000', '1529055304', '1', '测试账号', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_channel_account` VALUES ('230', '214', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '', '0.0000', '0.0000', '0.0000', '1529057574', '1', '100000057', '1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '0', '0', '0', '0', 's', '0', '0.00', '0.0000', '0.0000', '0.0000');

-- ----------------------------
-- Table structure for pay_complaints_deposit
-- ----------------------------
DROP TABLE IF EXISTS `pay_complaints_deposit`;
CREATE TABLE `pay_complaints_deposit` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `pay_orderid` varchar(100) NOT NULL DEFAULT '0' COMMENT '系统订单号',
  `out_trade_id` varchar(50) NOT NULL DEFAULT '' COMMENT '下游订单号',
  `freeze_money` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '冻结保证金额',
  `unfreeze_time` int(11) NOT NULL DEFAULT '0' COMMENT '计划解冻时间',
  `real_unfreeze_time` int(11) NOT NULL DEFAULT '0' COMMENT '实际解冻时间',
  `is_pause` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否暂停解冻 0正常解冻 1暂停解冻',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '解冻状态 0未解冻 1已解冻',
  `create_at` int(11) NOT NULL COMMENT '记录创建时间',
  `update_at` int(11) NOT NULL COMMENT '记录更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_unfreezeing` (`status`,`is_pause`,`unfreeze_time`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='投诉保证金余额';

-- ----------------------------
-- Records of pay_complaints_deposit
-- ----------------------------
INSERT INTO `pay_complaints_deposit` VALUES ('1', '2', '20180602235108995456', 'C20180602235107427887', '2.4800', '1530546725', '0', '0', '0', '1527954725', '1527954725');

-- ----------------------------
-- Table structure for pay_complaints_deposit_rule
-- ----------------------------
DROP TABLE IF EXISTS `pay_complaints_deposit_rule`;
CREATE TABLE `pay_complaints_deposit_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `is_system` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否系统规则 1是 0否',
  `ratio` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '投诉保证金比例（百分比）',
  `freeze_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '冻结时间（秒）',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '规则是否开启 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='投诉保证金规则表';

-- ----------------------------
-- Records of pay_complaints_deposit_rule
-- ----------------------------
INSERT INTO `pay_complaints_deposit_rule` VALUES ('1', '180586943', '1', '1.00', '0', '0');
INSERT INTO `pay_complaints_deposit_rule` VALUES ('2', '2', '0', '0.00', '0', '1');
INSERT INTO `pay_complaints_deposit_rule` VALUES ('3', '2', '0', '1.00', '3600', '1');

-- ----------------------------
-- Table structure for pay_df_api_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_df_api_order`;
CREATE TABLE `pay_df_api_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户编号',
  `trade_no` varchar(30) NOT NULL DEFAULT '' COMMENT '平台订单号',
  `out_trade_no` varchar(30) NOT NULL DEFAULT '' COMMENT '商户订单号',
  `money` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '金额',
  `bankname` varchar(100) NOT NULL DEFAULT '' COMMENT '银行名称',
  `subbranch` varchar(100) NOT NULL DEFAULT '' COMMENT '支行名称',
  `accountname` varchar(100) NOT NULL DEFAULT '' COMMENT '开户名',
  `cardnumber` varchar(100) NOT NULL DEFAULT '' COMMENT '银行卡号',
  `province` varchar(100) NOT NULL DEFAULT '' COMMENT '所属省',
  `city` varchar(100) NOT NULL DEFAULT '' COMMENT '所属市',
  `ip` varchar(100) DEFAULT '' COMMENT 'IP地址',
  `check_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：待审核 1：已提交后台审核 2：审核驳回',
  `extends` text COMMENT '扩展字段',
  `df_id` int(11) NOT NULL DEFAULT '0' COMMENT '代付ID',
  `notifyurl` varchar(255) DEFAULT '' COMMENT '异步通知地址',
  `reject_reason` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `check_time` int(11) NOT NULL DEFAULT '0' COMMENT '审核时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `df_charge_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '代付API扣除手续费方式，0：从到账金额里扣，1：从商户余额里扣',
  PRIMARY KEY (`id`),
  KEY `IND_UID` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_df_api_order
-- ----------------------------
INSERT INTO `pay_df_api_order` VALUES ('1', '2', 'H0601236061696254', '19700101080000', '100.0000', '1111', '111', '111', '111', '11', '111', '127.0.0.1', '0', '{\"lhh\":\"11\"}', '0', '', '', '0', '1527823606', '0');
INSERT INTO `pay_df_api_order` VALUES ('2', '2', 'H0601239597206183', '201806011132391669', '100.0000', '3345', '23423', '423423', '4234', '2432423', '423423', '127.0.0.1', '0', '{\"lhh\":\"234\"}', '0', '', '', '0', '1527823959', '0');
INSERT INTO `pay_df_api_order` VALUES ('3', '2', 'H0601241112282145', '201806011135118476', '100.0000', '3345', '23423', '423423', '4234', '2432423', '423423', '127.0.0.1', '0', '{\"lhh\":\"234\"}', '0', '', '', '0', '1527824111', '0');
INSERT INTO `pay_df_api_order` VALUES ('4', '2', 'H0601292798767714', '201806011301195354', '100.0000', '112121', '12121', '2121', '212121', '2121', '212121', '127.0.0.1', '1', '{\"lhh\":\"121\"}', '36', '', '', '1527845294', '1527829279', '0');
INSERT INTO `pay_df_api_order` VALUES ('13', '2', 'H0601303800250710', '201806011310335771', '100.0000', '112121', '12121', '2121', '212121', '2121', '212121', '127.0.0.1', '1', '{\"lhh\":\"121\"}', '0', '', '', '0', '1527830380', '0');
INSERT INTO `pay_df_api_order` VALUES ('22', '2', 'H0601308399422032', '201806011322112911', '100.0000', '112121', '12121', '2121', '212121', '2121', '212121', '127.0.0.1', '1', '{\"lhh\":\"121\"}', '33', '', '', '1527830840', '1527830839', '0');
INSERT INTO `pay_df_api_order` VALUES ('23', '2', 'H0601450678097402', '201806011724274583', '100.0000', '234234', '23423', '234234234', '23423', '234', '43223', '127.0.0.1', '2', '{\"lhh\":\"4234\"}', '34', '', '', '1528690002', '1527845067', '0');
INSERT INTO `pay_df_api_order` VALUES ('24', '2', 'H0601452019801950', '201806011726412997', '100.0000', '中国工商银行', '广州石牌支行', '马培坚', '23423', '234', '43223', '127.0.0.1', '2', '{\"lhh\":\"4234\"}', '35', '', '', '1528690002', '1527845201', '0');
INSERT INTO `pay_df_api_order` VALUES ('25', '2', 'H0619055370206327', '201806191852168827', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '42', '', '', '1529405537', '1529405537', '1');
INSERT INTO `pay_df_api_order` VALUES ('26', '2', 'H0619060458819310', '201806191900451071', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '43', '', '', '1529406045', '1529406045', '1');
INSERT INTO `pay_df_api_order` VALUES ('27', '2', 'H0619063721056252', '201806191906119790', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '44', '', '', '1529406372', '1529406372', '1');
INSERT INTO `pay_df_api_order` VALUES ('28', '2', 'H0619064979453326', '201806191908175000', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '45', '', '', '1529406498', '1529406497', '1');
INSERT INTO `pay_df_api_order` VALUES ('29', '2', 'H0619066034867955', '201806191910039650', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '0', '', '', '0', '1529406603', '0');
INSERT INTO `pay_df_api_order` VALUES ('30', '2', 'H0619066514687530', '201806191910513356', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '0', '', '', '0', '1529406651', '0');
INSERT INTO `pay_df_api_order` VALUES ('31', '2', 'H0619066931441930', '201806191911328822', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '0', '', '', '0', '1529406693', '0');
INSERT INTO `pay_df_api_order` VALUES ('32', '2', 'H0619067356265560', '201806191912154786', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '46', '', '', '1529406735', '1529406735', '0');
INSERT INTO `pay_df_api_order` VALUES ('33', '2', 'H0619068120014398', '201806191913317717', '100.0000', '1', '1', '1', '1', '1', '1', '127.0.0.1', '1', '{\"lhh\":\"1\"}', '47', '', '', '1529406812', '1529406812', '0');

-- ----------------------------
-- Table structure for pay_email
-- ----------------------------
DROP TABLE IF EXISTS `pay_email`;
CREATE TABLE `pay_email` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `smtp_host` varchar(300) DEFAULT NULL,
  `smtp_port` varchar(300) DEFAULT NULL,
  `smtp_user` varchar(300) DEFAULT NULL,
  `smtp_pass` varchar(300) DEFAULT NULL,
  `smtp_email` varchar(300) DEFAULT NULL,
  `smtp_name` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_email
-- ----------------------------
INSERT INTO `pay_email` VALUES ('1', 'smtpdm.aliyun.com', '465', 'info@mail.tianniu.cc', 'Mailpush123', 'info@mail.tianniu.cc', '知宇聚合API支付系统');

-- ----------------------------
-- Table structure for pay_invitecode
-- ----------------------------
DROP TABLE IF EXISTS `pay_invitecode`;
CREATE TABLE `pay_invitecode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invitecode` varchar(32) NOT NULL,
  `fmusernameid` int(11) unsigned NOT NULL DEFAULT '0',
  `syusernameid` int(11) NOT NULL DEFAULT '0',
  `regtype` tinyint(1) unsigned NOT NULL DEFAULT '4' COMMENT '用户组 4 普通用户 5 代理商',
  `fbdatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `yxdatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `sydatetime` int(11) unsigned DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '邀请码状态 0 禁用 1 未使用 2 已使用',
  `is_admin` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否管理员添加',
  PRIMARY KEY (`id`),
  UNIQUE KEY `invitecode` (`invitecode`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_invitecode
-- ----------------------------
INSERT INTO `pay_invitecode` VALUES ('12', '97xThB1Cz4OoV7y9OrdKW7HbmMDdeC', '1', '17', '4', '1491146221', '1491148800', '1491146782', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('13', 'XSR1xQqTuBAOflNTaGVJsShJe9ihpj', '1', '18', '4', '1491147225', '1491148800', '1491147268', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('5', 'Icrt3mdyaiwKtR9sEqqL9a43jU4qHI', '1', '7', '4', '1491064074', '1491148800', '1491069586', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('6', 'f2yOFZJcqjxzVYBITX1WKEVQbYycM5', '1', '8', '4', '1491069805', '1491148800', '1491069829', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('7', 'kqkPFdRhF4mHxbuGKnTrTUBOhh8BUR', '1', '9', '4', '1491100066', '1491148800', '1491100145', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('14', '8dCbyzAO1GasJ5Ycqtc4apoLIszGVs', '1', '19', '4', '1491149144', '1491235200', '1491149186', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('61', '9ucguw5j42hmpp9alrw83obs6ew070', '1', '0', '4', '1503653302', '1503739701', '0', '1', '1');
INSERT INTO `pay_invitecode` VALUES ('60', '6fbm325wa702pywe6d0voou70t5lz4', '1', '0', '4', '1503653299', '1503739698', '0', '1', '1');
INSERT INTO `pay_invitecode` VALUES ('28', 'GJhipszCqCRrFQTH5vQvOB3GZnBBb7', '19', '20', '4', '1491235513', '1491321600', '1491235668', '2', '0');
INSERT INTO `pay_invitecode` VALUES ('29', 'gElczzSWWpznTeXFPdyDbxgPNCvZb8', '1', '21', '4', '1491317272', '1491321600', '1491317286', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('30', 'um7KnpqaAapwrMjjqg4R5qh88zvLjZ', '1', '22', '4', '1491319745', '1491321600', '1491319789', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('31', 'ALP9duzS6BpSJgzRevRPY2iqFSxi4v', '1', '23', '4', '1491320343', '1491321600', '1491320387', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('32', 'k7IuU11htdy9AVSNZZFOzRIlSqUVkz', '1', '24', '4', '1491539877', '1491580800', '1491540040', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('33', 'SYMhPSCV2wLORE6ZrClUigccU6LsTp', '24', '25', '4', '1492017189', '1492099200', '1492017227', '2', '0');
INSERT INTO `pay_invitecode` VALUES ('58', 'y93ctdv8p27pq39788fjt6z78724iv', '1', '37', '4', '1503652737', '1503739136', '1503653097', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('43', '1swx1xq9b34kickbmwn3lfbowe1hj5', '1', '26', '4', '1499962964', '1500048000', '1499963193', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('44', 'nolpvni01tv174cupsn24lacuosge9', '1', '27', '4', '1500630828', '1500652800', '1500630876', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('59', '5pzj0lpb9o8nsf557f9ryl83dcgx42', '1', '0', '4', '1503653296', '1503739695', '0', '1', '1');
INSERT INTO `pay_invitecode` VALUES ('57', '6jym8c5eozmjahiky10jt6upb5l0px', '1', '38', '4', '1503652613', '1503739012', '1503653199', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('62', '9q2vrirdz3rym272vs8n5dfnv8m0qa', '1', '39', '4', '1503716035', '1503802434', '1503716302', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('63', 'd4tzgycfnpyktlenpbp7xk4tv22tyq', '1', '2', '4', '1503828843', '1503915242', '1503828960', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('64', 'vcpnps', '1', '3', '7', '1516440243', '1516526633', '1516440798', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('65', '6on1r7', '3', '4', '6', '1516440851', '1516527245', '1516440891', '2', '0');
INSERT INTO `pay_invitecode` VALUES ('66', 'kmrvm8', '4', '5', '5', '1516440924', '1516527320', '1516440963', '2', '0');
INSERT INTO `pay_invitecode` VALUES ('67', 'ht9qun', '5', '6', '4', '1516440996', '1516527392', '1516441030', '2', '0');
INSERT INTO `pay_invitecode` VALUES ('68', 'ryaern', '1', '180583931', '4', '1527695528', '1527781927', '1527695537', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('69', '6qy46f', '1', '180586944', '4', '1528105378', '1528191776', '1528105388', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('70', 'shlgjj', '1', '180586945', '4', '1528107817', '1528194215', '1528107823', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('71', 'rh8tfe', '1', '180586946', '4', '1528107908', '1528194307', '1528107925', '2', '1');
INSERT INTO `pay_invitecode` VALUES ('72', 'i11z1e', '2', '0', '4', '1528767366', '1528853765', '0', '1', '1');

-- ----------------------------
-- Table structure for pay_inviteconfig
-- ----------------------------
DROP TABLE IF EXISTS `pay_inviteconfig`;
CREATE TABLE `pay_inviteconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invitezt` tinyint(1) unsigned DEFAULT '1',
  `invitetype2number` int(11) NOT NULL DEFAULT '20',
  `invitetype2ff` smallint(6) NOT NULL DEFAULT '1',
  `invitetype5number` int(11) NOT NULL DEFAULT '20',
  `invitetype5ff` smallint(6) NOT NULL DEFAULT '1',
  `invitetype6number` int(11) NOT NULL DEFAULT '20',
  `invitetype6ff` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_inviteconfig
-- ----------------------------
INSERT INTO `pay_inviteconfig` VALUES ('1', '1', '0', '0', '10', '1', '0', '0');

-- ----------------------------
-- Table structure for pay_loginrecord
-- ----------------------------
DROP TABLE IF EXISTS `pay_loginrecord`;
CREATE TABLE `pay_loginrecord` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `logindatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `loginip` varchar(100) NOT NULL,
  `loginaddress` varchar(300) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型：0：前台用户 1：后台用户',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_loginrecord
-- ----------------------------
INSERT INTO `pay_loginrecord` VALUES ('71', '1', '2017-12-16 15:08:11', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('72', '1', '2018-01-06 09:43:15', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('73', '1', '2018-01-06 11:22:34', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('74', '1', '2018-01-08 11:34:23', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('75', '1', '2018-01-08 15:02:05', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('76', '1', '2018-01-18 19:12:32', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('77', '1', '2018-01-20 17:23:29', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('78', '1', '2018-01-20 17:26:02', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('79', '1', '2018-01-26 14:25:00', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('80', '1', '2018-02-26 18:13:31', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('81', '1', '2018-02-27 14:25:08', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('82', '1', '2018-03-01 10:27:54', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('83', '1', '2018-03-03 10:17:30', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('84', '1', '2018-04-24 11:24:37', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('85', '1', '2018-05-30 09:39:28', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('86', '1', '2018-05-31 09:19:31', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('87', '1', '2018-05-31 11:38:56', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('88', '1', '2018-06-01 09:23:47', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('89', '1', '2018-06-01 18:06:44', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('90', '1', '2018-06-01 18:31:30', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('91', '2', '2018-06-01 18:33:23', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('92', '2', '2018-06-01 18:34:58', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('93', '1', '2018-06-04 09:25:34', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('94', '1', '2018-06-05 19:00:55', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('95', '1', '2018-06-06 09:32:58', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('96', '1', '2018-06-07 10:03:08', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('97', '1', '2018-06-08 09:35:45', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('98', '1', '2018-06-09 09:16:50', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('99', '3', '2018-06-09 16:02:26', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('100', '3', '2018-06-09 16:03:45', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('101', '3', '2018-06-09 16:04:09', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('102', '3', '2018-06-09 16:04:24', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('103', '3', '2018-06-09 16:05:39', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('104', '1', '2018-06-11 09:26:41', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('105', '180586947', '2018-06-11 14:23:33', '127.0.0.1', '本机地址-', '0');
INSERT INTO `pay_loginrecord` VALUES ('106', '1', '2018-06-12 09:18:43', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('107', '2', '2018-06-12 09:35:29', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('108', '1', '2018-06-12 14:43:24', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('109', '1', '2018-06-13 09:38:27', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('110', '1', '2018-06-13 14:05:44', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('111', '2', '2018-06-13 19:08:38', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('112', '1', '2018-06-13 19:13:32', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('113', '2', '2018-06-13 19:14:45', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('114', '1', '2018-06-13 19:16:12', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('115', '1', '2018-06-13 19:23:47', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('116', '1', '2018-06-15 09:20:09', '127.0.0.1', '本机地址-', '1');
INSERT INTO `pay_loginrecord` VALUES ('117', '1', '2018-06-19 09:35:00', '127.0.0.1', '本机地址-', '1');

-- ----------------------------
-- Table structure for pay_member
-- ----------------------------
DROP TABLE IF EXISTS `pay_member`;
CREATE TABLE `pay_member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `groupid` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户组',
  `salt` varchar(10) NOT NULL COMMENT '密码随机字符',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '代理ID',
  `agent_cate` int(11) NOT NULL DEFAULT '0' COMMENT '代理级别',
  `balance` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '可用余额',
  `blockedbalance` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '冻结可用余额',
  `email` varchar(100) NOT NULL,
  `activate` varchar(200) NOT NULL,
  `regdatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `activatedatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `realname` varchar(50) DEFAULT NULL COMMENT '姓名',
  `sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '性别',
  `birthday` int(11) unsigned NOT NULL DEFAULT '0',
  `sfznumber` varchar(20) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL COMMENT '联系电话',
  `qq` varchar(15) DEFAULT NULL COMMENT 'QQ',
  `address` varchar(200) DEFAULT NULL COMMENT '联系地址',
  `paypassword` varchar(32) DEFAULT NULL COMMENT '支付密码',
  `authorized` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1 已认证 0 未认证 2 待审核',
  `apidomain` varchar(500) DEFAULT NULL COMMENT '授权访问域名',
  `apikey` varchar(32) NOT NULL COMMENT 'APIKEY',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1激活 0未激活',
  `receiver` varchar(255) DEFAULT NULL COMMENT '台卡显示的收款人信息',
  `unit_paying_number` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间已交易次数',
  `unit_paying_amount` decimal(11,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '单位时间已交易金额',
  `unit_frist_paying_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间已交易的第一笔时间',
  `last_paying_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天最后一笔已交易时间',
  `paying_money` decimal(11,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '当天已交易金额',
  `login_ip` varchar(255) NOT NULL DEFAULT ' ' COMMENT '登录IP',
  `last_error_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录错误时间',
  `login_error_num` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '错误登录次数',
  `google_auth` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启谷歌身份验证登录',
  `df_api` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启代付API',
  `open_charge` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启充值功能',
  `df_domain` text NOT NULL COMMENT '代付域名报备',
  `df_auto_check` tinyint(1) NOT NULL DEFAULT '0' COMMENT '代付API自动审核',
  `google_secret_key` varchar(255) NOT NULL DEFAULT '' COMMENT '谷歌密钥',
  `df_ip` text NOT NULL COMMENT '代付域名报备IP',
  `session_random` varchar(50) NOT NULL DEFAULT '' COMMENT 'session随机字符串',
  `df_charge_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '代付API扣除手续费方式，0：从到账金额里扣，1：从商户余额里扣',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=180586948 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_member
-- ----------------------------
INSERT INTO `pay_member` VALUES ('2', 'demouser', '0f69c07fc7967666fb2870349775751a', '4', '6450', '3', '0', '20426.8000', '14.9920', '551882927@qq.com', '8f9a2804950df5c2ff9ba1f8a9b64937', '1503828960', '2017', '曹军', '1', '1282838400', '440582198705204311', '15815295199', '', '中国', 'e10adc3949ba59abbe56e057f20f883e', '1', null, 't4ig5acnpx4fet4zapshjacjd9o4bhbi', '1', null, '3', '1.9920', '0', '1529391569', '1.9920', ' ', '1527849159', '0', '0', '1', '1', 'www.zuy.cn\r\nwww.baidu.cn\r\nwww.alipay.com\r\nwww.zuyapi.loc', '1', '', '69.172.89.61\r\n120.24.19.107\r\n127.0.0.1', '', '0');
INSERT INTO `pay_member` VALUES ('3', 'ceishi1', '7192f1fe70e36411dc90db51d5c35147', '7', '8577', '1', '0', '600.0030', '0.0000', 'ceishi1@qq.com', '296aa7fd77e854637ff15542ad1ba329', '1516440798', '0', null, '1', '0', null, null, null, null, 'e10adc3949ba59abbe56e057f20f883e', '1', null, 'axcxvdq0tqnqjvor93e57bo6t1ynpykw', '1', null, '0', '0.0000', '0', '0', '0.0000', ' ', '1528531587', '1', '0', '0', '0', '', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('4', 'ceishi2', '42eb548234c44a4165e36dbf4149bec0', '6', '5791', '3', '0', '700.0000', '0.0000', 'ceishi2@qq.com', 'fd007f16a134cfdf8c636ee953370983', '1516440891', '0', null, '1', '0', null, null, null, null, 'e10adc3949ba59abbe56e057f20f883e', '1', null, 'bt5neypv3849wvj58exlabu2yk0p9xle', '1', null, '0', '0.0000', '0', '0', '0.0000', ' ', '0', '0', '0', '0', '0', '', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('5', 'ceishi3', 'f92f32b9f44bd948ee6a06b676a92871', '7', '9833', '4', '0', '200.0000', '0.0000', 'ceishi3@qq.com', '6209d3c4247516f34417d0abba6a9f36', '1516440963', '0', null, '1', '0', null, null, null, null, 'e10adc3949ba59abbe56e057f20f883e', '1', null, 'q7aa42dgaxwtmw75s6rdolpplte6d269', '1', null, '0', '0.0000', '0', '0', '0.0000', ' ', '0', '0', '0', '1', '0', '', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('6', 'ceishi4', 'e2dd36a784d10e27494f17624d843b52', '4', '3064', '5', '0', '18000.0000', '0.0000', 'cheshi4@qq.com', 'decf50b881b0a5ad12d3759e1800f5c7', '1516441030', '0', null, '1', '0', null, null, null, null, 'e10adc3949ba59abbe56e057f20f883e', '1', null, 'lhd5d6miu1i5n99rfaiast79agn3o3ma', '1', null, '0', '0.0000', '0', '0', '0.0000', ' ', '0', '0', '0', '0', '0', '', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('180586943', 'mapeijian', 'f13725abab4b6854dc57902c7dff99f1', '4', '6730', '1', '4', '0.0000', '0.0000', '551882927@qq.com', '7991d59209aa1106f82c1ec3961513c8', '1527694510', '2018', '马培坚', '1', '1785600', '440582198705204311', '15815295199', '551882927', '广州', 'e10adc3949ba59abbe56e057f20f883e', '1', null, '6u83la7jllhoba6gpi0z0q1wk2ztphzz', '1', null, '0', '0.0000', '0', '0', '0.0000', '', '0', '0', '0', '1', '0', 'https://zuy.cn', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('180586944', 'mapeijian2', '91724b82f5d43023923d466e6ba5c2b0', '4', '2806', '1', '0', '0.0000', '0.0000', '551882927@qq.com', 'cb16ba38fd0b8d50bcc57c3365fdc9e7', '1528105388', '0', null, '1', '0', null, null, null, null, 'e10adc3949ba59abbe56e057f20f883e', '0', null, 'uzsrcupdsjw06hrbfshrggwgp5b6g5xy', '1', null, '0', '0.0000', '0', '0', '0.0000', ' ', '0', '0', '0', '0', '0', '', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('180586945', 'mapeijian3', '4f1164e505b7cc60f9e4fcd45e549439', '4', '8291', '1', '0', '0.0000', '0.0000', '551882927@qq.com', '60f9b265f624c6b552c56b54dc4469f0', '1528107823', '0', null, '1', '0', null, null, null, null, 'e10adc3949ba59abbe56e057f20f883e', '0', null, 'ot3oosmt58dn316ecqzbxtqyymk8hzha', '1', null, '0', '0.0000', '0', '0', '0.0000', ' ', '0', '0', '0', '0', '0', '', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('180586946', 'mapeijian5', '8bc7b8fdd5af088f6bffa9dc85da88c1', '4', '6520', '1', '0', '0.0000', '0.0000', '551882927@qq.com', 'd1af06acbd2965e76c64a15f62a82eaf', '1528107925', '2018', null, '1', '0', null, null, null, null, 'e10adc3949ba59abbe56e057f20f883e', '0', null, 'pqts9wh9b7m58tg5mx84911kf09iqcdv', '1', null, '0', '0.0000', '0', '0', '0.0000', ' ', '0', '0', '0', '0', '0', '', '0', '', '', '', '0');
INSERT INTO `pay_member` VALUES ('180586947', 'test', 'd242ec0d51a447c549e94958ebceecae', '4', '2023', '1', '4', '0.0000', '0.0000', '2287827147@qq.com', '2a13044648c71b41de0c7503f66eb2ba', '1528697733', '2018', 'test', '1', '548434800', '44058219870204311', '15815295199', '2287827147', '大学城', 'e10adc3949ba59abbe56e057f20f883e', '1', null, 'lunfsnxuklutr4frrgt7qnun3i1v0mtx', '1', null, '0', '0.0000', '0', '0', '0.0000', '', '1528698032', '0', '0', '1', '0', '', '1', '', '', '', '0');

-- ----------------------------
-- Table structure for pay_member_agent_cate
-- ----------------------------
DROP TABLE IF EXISTS `pay_member_agent_cate`;
CREATE TABLE `pay_member_agent_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(50) DEFAULT NULL COMMENT '等级名',
  `desc` varchar(255) DEFAULT NULL COMMENT '等级描述',
  `ctime` int(11) DEFAULT '0' COMMENT '添加时间',
  `sort` int(11) DEFAULT '99' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_member_agent_cate
-- ----------------------------
INSERT INTO `pay_member_agent_cate` VALUES ('4', '普通商户', '', '1522638122', '99');
INSERT INTO `pay_member_agent_cate` VALUES ('5', '普通代理商户', '', '1522638122', '99');
INSERT INTO `pay_member_agent_cate` VALUES ('6', '中级代理商户', '', '1522638122', '99');
INSERT INTO `pay_member_agent_cate` VALUES ('7', '高级代理商户', '', '1522638122', '99');

-- ----------------------------
-- Table structure for pay_moneychange
-- ----------------------------
DROP TABLE IF EXISTS `pay_moneychange`;
CREATE TABLE `pay_moneychange` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户编号',
  `ymoney` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '原金额',
  `money` decimal(15,4) NOT NULL DEFAULT '0.0000' COMMENT '变动金额',
  `gmoney` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '变动后金额',
  `datetime` datetime DEFAULT NULL COMMENT '修改时间',
  `transid` varchar(50) DEFAULT NULL COMMENT '交易流水号',
  `tongdao` smallint(6) unsigned DEFAULT '0' COMMENT '支付通道ID',
  `lx` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `tcuserid` int(11) DEFAULT NULL,
  `tcdengji` int(11) DEFAULT NULL,
  `orderid` varchar(50) DEFAULT NULL COMMENT '订单号',
  `contentstr` varchar(255) DEFAULT NULL COMMENT '备注',
  `t` int(4) NOT NULL DEFAULT '0' COMMENT '结算方式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_moneychange
-- ----------------------------
INSERT INTO `pay_moneychange` VALUES ('1', '2', '900.0000', '100.0000', '800.0000', '2018-05-30 10:11:21', 'H0530462819993974', '0', '6', null, null, 'H0530462819993974', '2018-05-30 10:11:21提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('2', '2', '800.0000', '100.0000', '700.0000', '2018-05-30 11:03:46', 'H0530494268984682', '0', '6', null, null, 'H0530494268984682', '2018-05-30 11:03:46委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('3', '2', '700.0000', '100.0000', '600.0000', '2018-05-30 11:27:50', 'H0530508708137860', '0', '6', null, null, 'H0530508708137860', '2018-05-30 11:27:50委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('4', '2', '600.0000', '200.0000', '400.0000', '2018-05-30 11:31:12', 'H0530510726156224', '0', '6', null, null, 'H0530510726156224', '2018-05-30 11:31:12委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('5', '2', '400.0000', '200.0000', '200.0000', '2018-05-30 11:35:09', 'H0530513095989090', '0', '6', null, null, 'H0530513095989090', '2018-05-30 11:35:09委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('6', '2', '20000.0000', '101.0000', '19899.0000', '2018-05-30 11:38:37', 'H0530515178959059', '0', '6', null, null, 'H0530515178959059', '2018-05-30 11:38:37委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('7', '2', '19899.0000', '102.0000', '19797.0000', '2018-05-30 11:38:37', 'H0530515178975627', '0', '6', null, null, 'H0530515178975627', '2018-05-30 11:38:37委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('8', '2', '19797.0000', '103.0000', '19694.0000', '2018-05-30 11:38:37', 'H0530515178985969', '0', '6', null, null, 'H0530515178985969', '2018-05-30 11:38:37委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('9', '2', '19694.0000', '104.0000', '19590.0000', '2018-05-30 11:38:37', 'H0530515178996043', '0', '6', null, null, 'H0530515178996043', '2018-05-30 11:38:37委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('13', '2', '19590.0000', '100.0000', '19490.0000', '2018-06-01 13:27:20', 'H0601308400200127', '0', '6', null, null, 'H0601308400200127', '2018-06-01 13:27:20委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('14', '2', '19490.0000', '100.0000', '19390.0000', '2018-06-01 17:24:27', 'H0601450678889614', '0', '6', null, null, 'H0601450678889614', '2018-06-01 17:24:27委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('15', '2', '19390.0000', '100.0000', '19290.0000', '2018-06-01 17:26:42', 'H0601452020606953', '0', '6', null, null, 'H0601452020606953', '2018-06-01 17:26:42委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('16', '2', '19290.0000', '100.0000', '19190.0000', '2018-06-01 17:28:14', 'H0601452942112740', '0', '6', null, null, 'H0601452942112740', '2018-06-01 17:28:14委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('17', '2', '0.0000', '0.0100', '0.0100', '2018-06-06 12:17:23', '20180604140857575297', '903', '1', null, null, 'E20180604140855329174', 'E20180604140855329174订单充值,结算方式：t+1', '0');
INSERT INTO `pay_moneychange` VALUES ('18', '2', '0.0000', '1.0000', '0.0000', '2018-06-08 09:45:02', '1', '0', '7', null, null, null, '测试', '0');
INSERT INTO `pay_moneychange` VALUES ('19', '2', '0.0000', '1.0000', '0.0000', '2018-06-08 11:28:01', '1', '0', '8', null, null, '', '自动解冻', '0');
INSERT INTO `pay_moneychange` VALUES ('20', '2', '0.0000', '1.0000', '0.0000', '2018-06-08 11:36:30', '1', '0', '8', null, null, '', '自动解冻', '0');
INSERT INTO `pay_moneychange` VALUES ('21', '2', '19191.0000', '1.0000', '19192.0000', '2018-06-08 12:07:36', '', '0', '3', null, null, '', '测试', '0');
INSERT INTO `pay_moneychange` VALUES ('22', '2', '19192.0000', '1.0000', '19193.0000', '2018-06-08 12:15:14', '1', '0', '7', null, null, null, '测试', '0');
INSERT INTO `pay_moneychange` VALUES ('23', '2', '19191.0000', '1.0000', '19192.0000', '2018-06-09 12:03:01', '', '0', '3', null, null, '', '测试冲正', '0');
INSERT INTO `pay_moneychange` VALUES ('24', '2', '19192.0000', '2.0000', '19194.0000', '2018-06-09 12:05:42', '', '0', '3', null, null, '', '测试【冲正周期\".2018-06-08】', '0');
INSERT INTO `pay_moneychange` VALUES ('25', '2', '19194.0000', '1.0000', '19195.0000', '2018-06-09 13:16:40', '1', '0', '7', null, null, null, '1', '0');
INSERT INTO `pay_moneychange` VALUES ('26', '2', '19193.0000', '1.0000', '19194.0000', '2018-06-09 14:08:38', '2', '0', '7', null, null, null, '测试', '0');
INSERT INTO `pay_moneychange` VALUES ('27', '2', '0.0000', '1.0000', '0.0000', '2018-06-09 14:32:06', null, null, '8', null, null, null, '手动冻结金额解冻', '0');
INSERT INTO `pay_moneychange` VALUES ('28', '2', '19193.0000', '2.0000', '19195.0000', '2018-06-09 14:59:17', '3', '0', '7', null, null, null, '11', '0');
INSERT INTO `pay_moneychange` VALUES ('29', '2', '0.0000', '2.0000', '0.0000', '2018-06-09 15:01:27', null, null, '8', null, null, null, '手动冻结金额解冻', '0');
INSERT INTO `pay_moneychange` VALUES ('30', '2', '19193.0000', '1.0000', '19194.0000', '2018-06-09 15:03:09', '4', '0', '7', null, null, null, '33', '0');
INSERT INTO `pay_moneychange` VALUES ('31', '2', '0.0000', '1.0000', '0.0000', '2018-06-09 15:04:56', null, null, '8', null, null, null, '手动冻结金额解冻', '0');
INSERT INTO `pay_moneychange` VALUES ('32', '2', '19193.0000', '2.0000', '19195.0000', '2018-06-09 15:05:08', '5', '0', '7', null, null, null, '2', '0');
INSERT INTO `pay_moneychange` VALUES ('33', '2', '0.0000', '2.0000', '0.0000', '2018-06-09 15:05:18', null, null, '8', null, null, null, '手动冻结金额解冻', '0');
INSERT INTO `pay_moneychange` VALUES ('34', '2', '19193.0000', '3.0000', '19196.0000', '2018-06-09 15:06:59', '6', '0', '7', null, null, null, '1', '0');
INSERT INTO `pay_moneychange` VALUES ('35', '2', '0.0000', '3.0000', '0.0000', '2018-06-09 15:07:10', null, null, '8', null, null, null, '手动冻结金额解冻', '0');
INSERT INTO `pay_moneychange` VALUES ('36', '2', '19193.0000', '11.0000', '19204.0000', '2018-06-09 15:09:40', '7', '0', '7', null, null, null, '111', '0');
INSERT INTO `pay_moneychange` VALUES ('43', '2', '19182.0000', '100.0000', '19282.0000', '2018-06-11 12:06:42', '24', '0', '12', null, null, '24', '商户代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('44', '2', '19282.0000', '100.0000', '19382.0000', '2018-06-11 12:06:42', '23', '0', '12', null, null, '23', '商户代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('45', '2', '19382.0000', '1.0000', '19383.0000', '2018-06-11 17:13:57', '', '0', '3', null, null, '', '11【冲正周期:2018-06-11】', '0');
INSERT INTO `pay_moneychange` VALUES ('46', '2', '19383.0000', '1.0000', '19384.0000', '2018-06-11 17:47:14', '', '0', '3', null, null, '', '1【冲正周期:2018-06-11】', '0');
INSERT INTO `pay_moneychange` VALUES ('47', '2', '19384.0000', '100.0000', '19484.0000', '2018-06-12 17:18:38', '1', '0', '11', null, null, '1', '结算驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('48', '2', '19484.0000', '100.0000', '19584.0000', '2018-06-12 17:53:02', '36', '0', '12', null, null, '36', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('49', '2', '19584.0000', '100.0000', '19684.0000', '2018-06-12 17:55:31', '36', '0', '12', null, null, '36', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('50', '2', '19684.0000', '100.0000', '19784.0000', '2018-06-12 17:56:11', '36', '0', '12', null, null, '36', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('51', '2', '19784.0000', '100.0000', '19884.0000', '2018-06-15 10:10:30', '1', '0', '11', null, null, '1', '结算驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('52', '2', '19884.0000', '100.0000', '19784.0000', '2018-06-19 10:10:07', 'H0619742079993539', '0', '6', null, null, 'H0619742079993539', '2018-06-19 10:10:07委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('53', '2', '19784.0000', '100.0000', '19684.0000', '2018-06-19 10:11:00', 'H0619742604038068', '0', '6', null, null, 'H0619742604038068', '2018-06-19 10:11:00委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('54', '2', '19684.0000', '100.0000', '19584.0000', '2018-06-19 10:11:41', 'H0619743012378377', '0', '6', null, null, 'H0619743012378377', '2018-06-19 10:11:41委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('55', '4', '600.0000', '100.0000', '700.0000', '2018-06-19 11:50:39', '', '0', '3', null, null, '', '11【冲正周期:2018-06-19】', '0');
INSERT INTO `pay_moneychange` VALUES ('56', '2', '14.0000', '0.9920', '14.9900', '2018-06-19 14:59:29', '20180615191459515352', '907', '1', null, null, 'E2018061519145633082', 'E2018061519145633082订单充值,结算方式：t+1', '0');
INSERT INTO `pay_moneychange` VALUES ('57', '3', '600.0000', '0.0030', '600.0030', '2018-06-19 14:59:29', '20180615191459515352', '907', '9', '2', '1', 'tx20180619145929', null, '0');
INSERT INTO `pay_moneychange` VALUES ('58', '2', '19584.0000', '100.1000', '19483.9000', '2018-06-19 15:45:51', 'H0619943515169736', '0', '6', null, null, 'H0619943515169736', '2018-06-19 15:45:51委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('59', '2', '19483.9000', '100.1000', '19383.8000', '2018-06-19 15:48:15', 'H0619944954648462', '0', '6', null, null, 'H0619944954648462', '2018-06-19 15:48:15委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('60', '2', '19383.8000', '1111.0000', '20494.8000', '2018-06-19 16:02:34', '', '0', '3', null, null, '', '1【冲正周期:】', '0');
INSERT INTO `pay_moneychange` VALUES ('61', '2', '20494.8000', '1.0000', '20495.8000', '2018-06-19 16:06:11', '', '0', '3', null, null, '', '1【冲正周期:】', '0');
INSERT INTO `pay_moneychange` VALUES ('62', '2', '20495.8000', '1.0000', '20496.8000', '2018-06-19 16:07:33', '', '0', '3', null, null, '', '1【冲正周期:2018-06-19】', '0');
INSERT INTO `pay_moneychange` VALUES ('63', '2', '20496.8000', '100.0000', '20396.8000', '2018-06-19 18:52:17', 'H0619055371166854', '0', '6', null, null, 'H0619055371166854', '2018-06-19 18:52:17委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('64', '2', '20396.8000', '10.0000', '20386.8000', '2018-06-19 18:52:17', 'H0619055371166854', '0', '14', null, null, 'H0619055371166854', '2018-06-19 18:52:17委托提现扣除手续费', '0');
INSERT INTO `pay_moneychange` VALUES ('65', '2', '20396.8000', '100.0000', '20496.8000', '2018-06-19 18:57:08', '42', '0', '12', null, null, '42', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('66', '2', '20496.8000', '100.0000', '20396.8000', '2018-06-19 19:00:45', 'H0619060459590870', '0', '6', null, null, 'H0619060459590870', '2018-06-19 19:00:45委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('67', '2', '20396.8000', '10.0000', '20386.8000', '2018-06-19 19:00:45', 'H0619060459590870', '0', '14', null, null, 'H0619060459590870', '2018-06-19 19:00:45委托提现扣除手续费', '0');
INSERT INTO `pay_moneychange` VALUES ('68', '2', '20396.8000', '100.0000', '20496.8000', '2018-06-19 19:01:51', '43', '0', '12', null, null, '43', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('69', '2', '20496.8000', '10.0000', '20506.8000', '2018-06-19 19:01:51', '43', '0', '15', null, null, '43', '结算驳回退回手续费', '0');
INSERT INTO `pay_moneychange` VALUES ('70', '2', '20506.8000', '100.0000', '20406.8000', '2018-06-19 19:06:12', 'H0619063721985868', '0', '6', null, null, 'H0619063721985868', '2018-06-19 19:06:12委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('71', '2', '20406.8000', '10.0000', '20396.8000', '2018-06-19 19:06:12', 'H0619063721985868', '0', '14', null, null, 'H0619063721985868', '2018-06-19 19:06:12委托提现扣除手续费', '0');
INSERT INTO `pay_moneychange` VALUES ('74', '2', '20406.8000', '100.0000', '20506.8000', '2018-06-19 19:07:35', '44', '0', '12', null, null, '44', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('75', '2', '20506.8000', '10.0000', '20516.8000', '2018-06-19 19:07:35', '44', '0', '15', null, null, '44', '结算驳回退回手续费', '0');
INSERT INTO `pay_moneychange` VALUES ('76', '2', '20516.8000', '100.0000', '20416.8000', '2018-06-19 19:08:18', 'H0619064980413981', '0', '6', null, null, 'H0619064980413981', '2018-06-19 19:08:18委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('77', '2', '20416.8000', '10.0000', '20406.8000', '2018-06-19 19:08:18', 'H0619064980413981', '0', '14', null, null, 'H0619064980413981', '2018-06-19 19:08:18委托提现扣除手续费', '0');
INSERT INTO `pay_moneychange` VALUES ('78', '2', '20416.8000', '100.0000', '20516.8000', '2018-06-19 19:08:40', '45', '0', '12', null, null, '45', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('79', '2', '20516.8000', '10.0000', '20526.8000', '2018-06-19 19:08:40', '45', '0', '15', null, null, '45', '结算驳回退回手续费', '0');
INSERT INTO `pay_moneychange` VALUES ('80', '2', '20526.8000', '100.0000', '20426.8000', '2018-06-19 19:12:15', 'H0619067357067854', '0', '6', null, null, 'H0619067357067854', '2018-06-19 19:12:15委托提现操作', '0');
INSERT INTO `pay_moneychange` VALUES ('81', '2', '20426.8000', '100.0000', '20526.8000', '2018-06-19 19:12:35', '46', '0', '12', null, null, '46', '代付驳回', '0');
INSERT INTO `pay_moneychange` VALUES ('82', '2', '20526.8000', '100.0000', '20426.8000', '2018-06-19 19:13:32', 'H0619068120771385', '0', '6', null, null, 'H0619068120771385', '2018-06-19 19:13:32委托提现操作', '0');

-- ----------------------------
-- Table structure for pay_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pay_memberid` varchar(100) NOT NULL COMMENT '商户编号',
  `pay_orderid` varchar(100) NOT NULL COMMENT '系统订单号',
  `pay_amount` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `pay_poundage` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `pay_actualamount` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `pay_applydate` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单创建日期',
  `pay_successdate` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单支付成功时间',
  `pay_bankcode` varchar(100) DEFAULT NULL COMMENT '银行编码',
  `pay_notifyurl` varchar(500) NOT NULL COMMENT '商家异步通知地址',
  `pay_callbackurl` varchar(500) NOT NULL COMMENT '商家页面通知地址',
  `pay_bankname` varchar(300) DEFAULT NULL,
  `pay_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '订单状态: 0 未支付 1 已支付未返回 2 已支付已返回',
  `pay_productname` varchar(300) DEFAULT NULL COMMENT '商品名称',
  `pay_tongdao` varchar(50) DEFAULT NULL,
  `pay_zh_tongdao` varchar(50) DEFAULT NULL,
  `pay_tjurl` varchar(1000) DEFAULT NULL,
  `out_trade_id` varchar(50) NOT NULL COMMENT '商户订单号',
  `num` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '已补发次数',
  `memberid` varchar(100) DEFAULT NULL COMMENT '支付渠道商家号',
  `key` varchar(500) DEFAULT NULL COMMENT '支付渠道密钥',
  `account` varchar(100) DEFAULT NULL COMMENT '渠道账号',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '伪删除订单 1 删除 0 未删',
  `ddlx` int(11) DEFAULT '0',
  `pay_ytongdao` varchar(50) DEFAULT NULL,
  `pay_yzh_tongdao` varchar(50) DEFAULT NULL,
  `xx` smallint(6) unsigned NOT NULL DEFAULT '0',
  `attach` text CHARACTER SET utf8mb4 COMMENT '商家附加字段,原样返回',
  `pay_channel_account` varchar(255) DEFAULT NULL COMMENT '通道账户',
  `cost` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '成本',
  `cost_rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '成本费率',
  `account_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '子账号id',
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '渠道id',
  `t` tinyint(2) NOT NULL DEFAULT '1' COMMENT '结算周期（计算费率）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IND_ORD` (`pay_orderid`),
  KEY `account_id` (`account_id`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=491 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_order
-- ----------------------------
INSERT INTO `pay_order` VALUES ('1', '10002', '20180402173845535055', '100.0000', '0.8000', '99.2000', '1522661925', '1522661985', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402173840733966', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('2', '10002', '20180402173904565553', '100.0000', '0.8000', '99.2000', '1522661944', '1522662004', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402173840733966', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('3', '10002', '20180402175630101545', '100.0000', '0.8000', '99.2000', '1522662990', '1522663050', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402175626942047', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('4', '10002', '20180402175924991019', '100.0000', '0.8000', '99.2000', '1522663164', '1522663224', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402175922673239', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('5', '10002', '20180402180708991001', '100.0000', '0.8000', '99.2000', '1522663628', '1522663688', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402180706546951', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('6', '10002', '20180402181220524856', '100.0000', '0.8000', '99.2000', '1522663940', '1522664000', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402181217619076', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('7', '10002', '20180402181417575598', '100.0000', '0.8000', '99.2000', '1522664057', '1522664117', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402181415889752', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('8', '10002', '20180402184358101541', '100.0000', '0.8000', '99.2000', '1522665838', '1522665898', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdquickpay', '衫德快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402182022880194', '0', '18776150', '', '', '0', '0', 'Sdquickpay', '衫德快捷支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '225', '208', '1');
INSERT INTO `pay_order` VALUES ('9', '10002', '20180402194903102499', '100.0000', '0.8000', '99.2000', '1522669743', '1522669803', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402194900758740', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('10', '10002', '20180402194948994852', '100.0000', '0.8000', '99.2000', '1522669788', '1522669848', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402194945350296', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('11', '10002', '20180402195046544999', '100.0000', '0.8000', '99.2000', '1522669846', '1522669906', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402195043795846', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('12', '10002', '20180402205651515752', '100.0000', '0.8000', '99.2000', '1522673811', '1522673871', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402205649932269', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('13', '10002', '20180402205950545448', '100.0000', '0.8000', '99.2000', '1522673990', '1522674050', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402205947634622', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('14', '10002', '20180402210420529849', '100.0000', '0.8000', '99.2000', '1522674260', '1522674320', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402210416362326', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('15', '10002', '20180402210949100579', '100.0000', '0.8000', '99.2000', '1522674589', '1522674649', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402210943433078', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('16', '10002', '20180402211310545650', '100.0000', '0.8000', '99.2000', '1522674790', '1522674850', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402211307993930', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('17', '10002', '20180402213327555554', '100.0000', '0.8000', '99.2000', '1522676007', '1522676067', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402213300646130', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('18', '10002', '20180402213835985754', '100.0000', '0.8000', '99.2000', '1522676315', '1522676375', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402213831972753', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('19', '10002', '20180402214555515548', '100.0000', '0.8000', '99.2000', '1522676755', '1522676815', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402214552359167', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('20', '10002', '20180402214907515410', '100.0000', '0.8000', '99.2000', '1522676947', '1522677007', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180402214714250402', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('21', '10002', '20180404110443985348', '100.0000', '0.8000', '99.2000', '1522811083', '1522811143', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404110439766787', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('22', '10002', '20180404110543554955', '100.0000', '0.8000', '99.2000', '1522811143', '1522811203', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404110540966793', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('23', '10002', '20180404110811985010', '100.0000', '0.8000', '99.2000', '1522811291', '1522811351', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404110804264767', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('24', '10002', '20180404113112485450', '100.0000', '0.8000', '99.2000', '1522812672', '1522812732', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404113109309921', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('25', '10002', '20180404113356524999', '100.0000', '0.8000', '99.2000', '1522812836', '1522812896', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404113352698699', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('26', '10002', '20180404145554975752', '6.0000', '0.0500', '5.9500', '1522824954', '1522825014', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404145552820785', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('27', '10002', '20180404145649491015', '10.0000', '0.0800', '9.9200', '1522825009', '1522825069', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404145647235818', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('28', '10002', '20180404145702101100', '9.0000', '0.0700', '8.9300', '1522825022', '1522825082', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404145700658654', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('29', '10002', '20180404145722504851', '10.1000', '0.0800', '10.0200', '1522825042', '1522825102', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404145720266717', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('30', '10002', '20180404145734101994', '10.0100', '0.0800', '9.9300', '1522825054', '1522825114', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'Sdwgpay', '衫德网关支付', 'http://www.payapi.loc/demo/index1.php', 'E20180404145732526901', '0', '18776150', '', '', '0', '0', 'Sdwgpay', '衫德网关支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '226', '209', '1');
INSERT INTO `pay_order` VALUES ('31', '10002', '20180411174459989957', '100.0000', '0.8000', '99.2000', '1523439899', '1523439959', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180411174457534619', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(网银支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('32', '10002', '20180411174532995053', '100.0000', '0.8000', '99.2000', '1523439932', '1523439992', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180411174457534619', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(网银支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('33', '10002', '20180411174615555057', '100.0000', '0.8000', '99.2000', '1523439975', '1523440035', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180411174457534619', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(网银支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('34', '10002', '20180411175842501015', '100.0000', '0.8000', '99.2000', '1523440723', '1523440783', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180411174457534619', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(网银支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('35', '10002', '20180411181602505452', '100.0000', '0.8000', '99.2000', '1523441762', '1523441822', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo/index1.php', 'E20180411181559384902', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvQuick', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('36', '10002', '20180412115536564998', '1.0000', '0.0100', '0.9900', '1523505336', '1523505396', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115534286877', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvQuick', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('37', '10002', '20180412115617495050', '1.0000', '0.0100', '0.9900', '1523505377', '1523505437', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('38', '10002', '20180412115644994849', '1.0000', '0.0100', '0.9900', '1523505404', '1523505464', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('39', '10002', '20180412115656569910', '1.0000', '0.0100', '0.9900', '1523505416', '1523505476', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('40', '10002', '20180412115720481015', '1.0000', '0.0100', '0.9900', '1523505440', '1523505500', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('41', '10002', '20180412115826505349', '1.0000', '0.0100', '0.9900', '1523505506', '1523505566', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('42', '10002', '20180412115904565248', '1.0000', '0.0100', '0.9900', '1523505544', '1523505604', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('43', '10002', '20180412115905575656', '1.0000', '0.0100', '0.9900', '1523505545', '1523505605', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('44', '10002', '20180412115932525297', '1.0000', '0.0100', '0.9900', '1523505572', '1523505632', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('45', '10002', '20180412120008565251', '1.0000', '0.0100', '0.9900', '1523505608', '1523505668', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('46', '10002', '20180412120011985698', '1.0000', '0.0100', '0.9900', '1523505611', '1523505671', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('47', '10002', '20180412120012991015', '1.0000', '0.0100', '0.9900', '1523505612', '1523505672', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('48', '10002', '20180412120013100991', '1.0000', '0.0100', '0.9900', '1523505613', '1523505673', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('49', '10002', '20180412120014101985', '1.0000', '0.0100', '0.9900', '1523505614', '1523505674', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('50', '10002', '20180412120015102564', '1.0000', '0.0100', '0.9900', '1523505615', '1523505675', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('51', '10002', '20180412120016485198', '1.0000', '0.0100', '0.9900', '1523505616', '1523505676', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('52', '10002', '20180412120023555655', '1.0000', '0.0100', '0.9900', '1523505623', '1523505683', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120021947814', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('53', '10002', '20180412120045100535', '1.0000', '0.0100', '0.9900', '1523505645', '1523505705', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120021947814', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('54', '10002', '20180412120046101100', '1.0000', '0.0100', '0.9900', '1523505646', '1523505706', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120021947814', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('55', '10002', '20180412120056565097', '1.0000', '0.0100', '0.9900', '1523505656', '1523505716', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120021947814', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('56', '10002', '20180412120140525152', '1.0000', '0.0100', '0.9900', '1523505700', '1523505760', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120021947814', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('57', '10002', '20180412120141535399', '1.0000', '0.0100', '0.9900', '1523505701', '1523505761', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120021947814', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('58', '10002', '20180412120142545599', '1.0000', '0.0100', '0.9900', '1523505702', '1523505762', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120021947814', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('59', '10002', '20180412120143555650', '1.0000', '0.0100', '0.9900', '1523505703', '1523505763', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('60', '10002', '20180412120145579957', '1.0000', '0.0100', '0.9900', '1523505705', '1523505765', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('61', '10002', '20180412120147985198', '1.0000', '0.0100', '0.9900', '1523505707', '1523505767', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412115608323489', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('62', '10002', '20180412120514975410', '1.0000', '0.0100', '0.9900', '1523505914', '1523505974', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvBank', 'PAYCV网银支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120512296408', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(网银支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '227', '210', '1');
INSERT INTO `pay_order` VALUES ('63', '10002', '20180412120555511024', '1.0000', '0.0100', '0.9900', '1523505956', '1523506016', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120553575515', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('64', '10002', '20180412120637100525', '1.0000', '0.0100', '0.9900', '1523505997', '1523506057', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120635487020', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('65', '10002', '20180412120729495049', '1.0000', '0.0100', '0.9900', '1523506049', '1523506109', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120635487020', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('66', '10002', '20180412120744485710', '1.0000', '0.0100', '0.9900', '1523506064', '1523506124', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120635487020', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('67', '10002', '20180412120804524998', '1.0000', '0.0100', '0.9900', '1523506084', '1523506144', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120635487020', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('68', '10002', '20180412120805535798', '1.0000', '0.0100', '0.9900', '1523506085', '1523506145', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120635487020', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('69', '10002', '20180412120808569810', '1.0000', '0.0100', '0.9900', '1523506088', '1523506148', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120806319644', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('70', '10002', '20180412120929574810', '1.0000', '0.0100', '0.9900', '1523506169', '1523506229', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://www.payapi.loc/demo4/index1.php', 'E20180412120926420114', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('71', '10002', '20180412121301100102', '1.0000', '0.0100', '0.9900', '1523506382', '1523506442', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://pay.rcywlkj.com/demo4/index1.php', 'E20180412121259552441', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('72', '10002', '20180412161610975598', '0.0100', '0.0000', '0.0100', '1523520970', '1523521030', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://pay.rcywlkj.com/demo4/index1.php', 'E20180412161608990511', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('73', '10002', '20180412162209495410', '0.1000', '0.0000', '0.1000', '1523521329', '1523521389', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://pay.rcywlkj.com/demo4/index1.php', 'E20180412162207192858', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('74', '10002', '20180412162520489810', '0.1000', '0.0000', '0.1000', '1523521520', '1523521580', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://pay.rcywlkj.com/demo4/index1.php', 'E20180412162518684606', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('75', '10002', '20180412162729491009', '1.0000', '0.0100', '0.9900', '1523521649', '1523521709', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'CvQuick', 'PAYCV快捷支付', 'http://pay.rcywlkj.com/demo4/index1.php', 'E20180412162726169449', '0', '10124', 'h0ecqvphc483ogk1lt8148jwrrkjnb0j', '', '0', '0', 'CvBank', 'PAYCV(D0快捷支付)', '0', '1234|456', '正式账号', '0.0000', '0.0000', '228', '211', '1');
INSERT INTO `pay_order` VALUES ('76', '10002', '20180416151456481015', '100.0000', '0.8000', '99.2000', '1523862896', '1523862956', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'HxBank', '环迅网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180416151435932681', '0', '207231', 'Mo6HqQtlMv4j9hZwfRck7lVGSd8pG9C75ascLsjgVgw88Yv4v0kVEVpMmFcDESGMpyZuclb4p7HB2oAwXeAk9UYq0RsllfcwTAfpvROpC0jMLn9sJWlg9HMCxhhNpVrk', '', '0', '0', 'HxBank', '环迅网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '229', '212', '1');
INSERT INTO `pay_order` VALUES ('77', '10002', '20180416154051515552', '100.0000', '0.8000', '99.2000', '1523864451', '1523864511', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'HxBank', '环迅网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180416154049288470', '0', '207231', 'Mo6HqQtlMv4j9hZwfRck7lVGSd8pG9C75ascLsjgVgw88Yv4v0kVEVpMmFcDESGMpyZuclb4p7HB2oAwXeAk9UYq0RsllfcwTAfpvROpC0jMLn9sJWlg9HMCxhhNpVrk', '2072310010', '0', '0', 'HxBank', '环迅网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '229', '212', '1');
INSERT INTO `pay_order` VALUES ('78', '10002', '20180416154527559897', '100.0000', '0.8000', '99.2000', '1523864727', '1523864787', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'HxBank', '环迅网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180416154526219393', '0', '207231', 'Mo6HqQtlMv4j9hZwfRck7lVGSd8pG9C75ascLsjgVgw88Yv4v0kVEVpMmFcDESGMpyZuclb4p7HB2oAwXeAk9UYq0RsllfcwTAfpvROpC0jMLn9sJWlg9HMCxhhNpVrk', '2072310010', '0', '0', 'HxBank', '环迅网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '229', '212', '1');
INSERT INTO `pay_order` VALUES ('79', '10002', '20180416164853535056', '100.0000', '0.8000', '99.2000', '1523868533', '1523868593', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'HxBank', '环迅网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180416154526219393', '0', '207231', 'Mo6HqQtlMv4j9hZwfRck7lVGSd8pG9C75ascLsjgVgw88Yv4v0kVEVpMmFcDESGMpyZuclb4p7HB2oAwXeAk9UYq0RsllfcwTAfpvROpC0jMLn9sJWlg9HMCxhhNpVrk', '2072310010', '0', '0', 'HxBank', '环迅网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '229', '212', '1');
INSERT INTO `pay_order` VALUES ('80', '10002', '20180416180142545298', '0.1000', '0.0000', '0.1000', '1523872902', '1523872962', '907', 'http://6shun.zlaiaa.top:1818/demo/server.php', 'http://6shun.zlaiaa.top:1818/demo/page.php', '网银支付', '2', 'VIP基础服务', 'HxBank', '环迅网银支付', 'http://6shun.zlaiaa.top:1818/demo2/index1.php', 'E20180416180141395430', '0', '207231', 'Mo6HqQtlMv4j9hZwfRck7lVGSd8pG9C75ascLsjgVgw88Yv4v0kVEVpMmFcDESGMpyZuclb4p7HB2oAwXeAk9UYq0RsllfcwTAfpvROpC0jMLn9sJWlg9HMCxhhNpVrk', '2072310010', '0', '0', 'HxBank', '环迅网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('81', '10002', '20180425181309539799', '100.0000', '0.5000', '99.5000', '1524651189', '1524651249', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180425181102407562', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('82', '10002', '20180425181332999910', '100.0000', '0.5000', '99.5000', '1524651212', '1524651272', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180425181330165451', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('83', '10002', '20180426091113495155', '100.0000', '0.5000', '99.5000', '1524705073', '1524705133', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426090929499792', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('84', '10002', '20180426091333100531', '0.0100', '0.0000', '0.0100', '1524705213', '1524705273', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091314520584', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('85', '10002', '20180426091439102975', '0.0100', '0.0000', '0.0100', '1524705279', '1524705339', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091437305361', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('86', '10002', '20180426091457495499', '0.1000', '0.0000', '0.1000', '1524705297', '1524705357', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091451590704', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('87', '10002', '20180426091511102971', '1.0000', '0.0100', '1.0000', '1524705311', '1524705371', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091509480209', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('88', '10002', '20180426091521575154', '10.0000', '0.0500', '9.9500', '1524705321', '1524705381', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091519656457', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('89', '10002', '20180426091702101549', '10.0000', '0.0500', '9.9500', '1524705422', '1524705482', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091659736685', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('90', '10002', '20180426091711555349', '10.0000', '0.0500', '9.9500', '1524705431', '1524705491', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091707453567', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('91', '10002', '20180426091821100579', '10.0000', '0.0500', '9.9500', '1524705501', '1524705561', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091819877941', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('92', '10002', '20180426091848565748', '10.0000', '0.0500', '9.9500', '1524705528', '1524705588', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426091847906890', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('93', '10002', '20180426092555515053', '10.0000', '0.0500', '9.9500', '1524705955', '1524706015', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426092551387045', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('94', '10002', '20180426092616565710', '10.0000', '0.0500', '9.9500', '1524705976', '1524706036', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426092614186654', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('95', '10002', '20180426093413539749', '10.0000', '0.0500', '9.9500', '1524706453', '1524706513', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426093409510229', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('96', '10002', '20180426093506974910', '10.0000', '0.0500', '9.9500', '1524706506', '1524706566', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426093504328048', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('97', '10002', '20180426093722509710', '10.0000', '0.0500', '9.9500', '1524706642', '1524706702', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426093720672937', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('98', '10005', '20180426093849579851', '10.0000', '0.0500', '9.9500', '1524706729', '1524706789', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426093848395449', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('99', '10005', '20180426094029100100', '10.0000', '0.0500', '9.9500', '1524706829', '1524706889', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426094027747314', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('100', '10002', '20180426094058975151', '10.0000', '0.0500', '9.9500', '1524706858', '1524706918', '902', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '微信扫码支付', '2', 'Recharge', 'Qydwxscan', '奇易达微信扫码', 'http://www.payapi.loc/demo/index1.php', 'E20180426094056156881', '0', '2146', 'b47f0b57bfd74b51b38a54795fc0e84c', '', '0', '0', 'Qydwxscan', '奇易达微信扫码', '0', '1234|456', '友付', '0.0000', '0.0000', '230', '213', '1');
INSERT INTO `pay_order` VALUES ('101', '10002', '20180427122214541005', '10.0000', '0.0800', '9.9200', '1524802934', '1524802994', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427122111287619', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('102', '10002', '20180427130815102100', '10.0000', '0.0800', '9.9200', '1524805695', '1524805755', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427130811766101', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('103', '10002', '20180427132034509797', '10.0000', '0.0800', '9.9200', '1524806434', '1524806494', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427132030452661', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('104', '10002', '20180427132548995351', '10.0000', '0.0800', '9.9200', '1524806748', '1524806808', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427132544353234', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('105', '10002', '20180427154246545454', '10.0000', '0.0800', '9.9200', '1524814966', '1524815026', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427154241535415', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('106', '10006', '20180427154543555755', '10.0000', '0.0800', '9.9200', '1524815143', '1524815203', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427154539694442', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('107', '10006', '20180427192512564953', '10.0000', '0.0800', '9.9200', '1524828312', '1524828372', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427192509147598', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('108', '10002', '20180427192603984957', '10.0000', '0.0800', '9.9200', '1524828363', '1524828423', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427192600269464', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('109', '10002', '20180427192736565249', '10.0000', '0.0800', '9.9200', '1524828456', '1524828516', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427192635203353', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('110', '10002', '20180427193334101101', '10.0000', '0.0800', '9.9200', '1524828814', '1524828874', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427193331640884', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('111', '10002', '20180427194320565757', '10.0000', '0.0800', '9.9200', '1524829400', '1524829460', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427194316427117', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('112', '10002', '20180427194337579752', '10.0000', '0.0800', '9.9200', '1524829417', '1524829477', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427194334362957', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('113', '10002', '20180427205223555555', '10.0000', '0.0800', '9.9200', '1524833543', '1524833603', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427205219770028', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('114', '10003', '20180427210408569798', '10.0000', '0.0800', '9.9200', '1524834248', '1524834308', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427210406622097', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('115', '10003', '20180427210503102565', '10.0000', '0.0800', '9.9200', '1524834303', '1524834363', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427210501633743', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('116', '10003', '20180427210747514810', '10.0000', '0.0800', '9.9200', '1524834467', '1524834527', '907', 'http://www.payapi.loc/demo/server.php', 'http://www.payapi.loc/demo/page.php', '网银支付', '2', 'Recharge', 'KmBank', '快码网银支付', 'http://www.payapi.loc/demo/index1.php', 'E20180427210737919937', '0', '17296', '27c89d1f24d7f9a13391476a66f170f6', '', '0', '0', 'KmBank', '快码网银支付', '0', '1234|456', '正式账号', '0.0000', '0.0000', '231', '214', '1');
INSERT INTO `pay_order` VALUES ('378', '10003', '20180530191227981019', '100.0000', '0.5000', '99.5000', '1527678747', '1527678807', '902', 'http://www.zuyapi.loc/Pay_Charges_notify.php', 'http://www.zuyapi.loc/Pay_Charges_callback.php', '微信扫码支付', '2', '收款', 'WxSm', '微信扫码支付', 'http://www.zuyapi.loc/Pay_Charges_checkout.html', 'C20180530191227239361', '0', '', '', '', '0', '0', 'WxSm', '微信扫码支付-官方', '0', '', '微信扫码支付', '0.0000', '0.0000', '218', '199', '1');
INSERT INTO `pay_order` VALUES ('379', '10003', '20180530193358549751', '111.0000', '0.0000', '111.0000', '1527680038', '1527680098', '903', 'http://www.zuyapi.loc/Pay_Charges_notify.php', 'http://www.zuyapi.loc/Pay_Charges_callback.php', '支付宝扫码支付', '2', '收款', 'Aliscan', '支付宝扫码', 'http://www.zuyapi.loc/Pay_Charges_checkout.html', 'C20180530193358592709', '0', '', '', '', '0', '0', 'Aliscan', '支付宝官方扫码', '0', 'fdgdfg', '支付宝扫码', '0.0000', '0.0000', '220', '201', '1');
INSERT INTO `pay_order` VALUES ('380', '10002', '20180530193433575655', '22.0000', '0.0000', '22.0000', '1527680073', '1527680133', '904', 'http://www.zuyapi.loc/Pay_Charges_notify.php', 'http://www.zuyapi.loc/Pay_Charges_callback.php', '支付宝手机', '2', '收款', 'Aliwap', '支付宝H5', 'http://www.zuyapi.loc/Pay_Charges_checkout.html', 'C20180530193433972836', '0', '', '', '', '0', '0', 'Aliwap', '支付宝官方（WAP）', '0', '', '支付宝H5', '0.0000', '0.0000', '221', '202', '1');
INSERT INTO `pay_order` VALUES ('381', '180596943', '20180530193442505299', '55.0000', '0.0000', '55.0000', '1527680082', '1527680142', '904', 'http://www.zuyapi.loc/Pay_Charges_notify.php', 'http://www.zuyapi.loc/Pay_Charges_callback.php', '支付宝手机', '2', '收款', 'Aliwap', '支付宝H5', 'http://www.zuyapi.loc/Pay_Charges_checkout.html', 'C20180530193442581942', '0', '', '', '', '0', '0', 'Aliwap', '支付宝官方（WAP）', '0', '', '支付宝H5', '0.0000', '0.0000', '221', '202', '1');
INSERT INTO `pay_order` VALUES ('382', '180596943', '20180530235017574953', '100.0000', '4.0000', '96.0000', '1527695417', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '2', 'VIP基础服务', 'WxSm', '微信扫码支付', 'http://www.zuyapi.loc/demo/index1.php', 'E20180530235015435714', '0', '', '', '', '0', '0', 'WxSm', '微信扫码支付-官方', '0', '1234|456', '微信扫码支付', '0.0000', '0.0000', '218', '199', '1');
INSERT INTO `pay_order` VALUES ('383', '10002', '20180531115508994810', '0.0100', '0.0000', '0.0100', '1527738908', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115506988546', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('384', '10002', '20180531115532525155', '0.0100', '0.0000', '0.0100', '1527738932', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115530485263', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('385', '10002', '20180531115556999810', '0.0100', '0.0000', '0.0100', '1527738956', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115530485263', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('386', '10002', '20180531115604525152', '0.0100', '0.0000', '0.0100', '1527738964', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115602484356', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('387', '10002', '20180531115619515799', '0.0100', '0.0000', '0.0100', '1527738979', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115618217059', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('388', '10002', '20180531115703102515', '0.0100', '0.0000', '0.0100', '1527739023', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115701262597', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('389', '10002', '20180531115724525048', '0.0100', '0.0000', '0.0100', '1527739044', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115722596252', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('390', '10002', '20180531115730975098', '0.0100', '0.0000', '0.0100', '1527739050', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115722596252', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('391', '10002', '20180531115748995551', '0.0100', '0.0000', '0.0100', '1527739068', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115722596252', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('392', '10002', '20180531115756529898', '0.0100', '0.0000', '0.0100', '1527739076', '0', '904', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '支付宝手机', '2', 'VIP基础服务', 'AliwapOld', '支付宝WAP（老版）', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531115752680902', '0', '2088131085175285', 'tp7msh810uw5p2jga2bcmrub63ldydob', 'yeah198800@126.com', '0', '0', 'Aliwap', '支付宝官方（老版WAP）', '0', '1234|456', '2088131085175285', '0.0000', '0.0000', '224', '208', '1');
INSERT INTO `pay_order` VALUES ('393', '180596943', '20180531152815102100', '100.0000', '0.0000', '100.0000', '1527751695', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531152813227139', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('394', '180596943', '20180531153103554910', '100.0000', '0.0000', '100.0000', '1527751863', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531153101110519', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('395', '180596943', '20180531153113495157', '100.0000', '0.0000', '100.0000', '1527751873', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531153110892224', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('396', '180596943', '20180531153144489853', '100.0000', '0.0000', '100.0000', '1527751904', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531153143568319', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('397', '180596943', '20180531153336489853', '100.0000', '0.0000', '100.0000', '1527752016', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531153335222744', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('398', '180596943', '20180531153519555252', '100.0000', '0.0000', '100.0000', '1527752119', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531153517137243', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('399', '180596943', '20180531153756529810', '100.0000', '0.0000', '100.0000', '1527752276', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531153755585430', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('400', '180596943', '20180531153807102535', '100.0000', '0.0000', '100.0000', '1527752287', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531153755585430', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('401', '180596943', '20180531161941100575', '100.0000', '0.0000', '100.0000', '1527754781', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531161939593725', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('402', '180596943', '20180531162002505510', '100.0000', '0.0000', '100.0000', '1527754802', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531162001335409', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('403', '180596943', '20180531163932525048', '100.0000', '0.0000', '100.0000', '1527755972', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'LzfBank', '乐支付网银', 'http://www.zuyapi.loc/demo/index1.php', 'E20180531163929856518', '0', '10003', '261b49f58832cb5447420e7bc9b977e0', '', '0', '0', 'LzfBank', '乐支付（网银）', '0', '1234|456', '10003', '0.0000', '0.0000', '225', '209', '1');
INSERT INTO `pay_order` VALUES ('444', '10002', '20180613173028521015', '1.0000', '0.0050', '0.9950', '1528882228', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317302769188', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('445', '10002', '20180613173104565549', '1.0000', '0.0050', '0.9950', '1528882264', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317310150319', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('446', '10002', '20180613173142101485', '1.0000', '0.0050', '0.9950', '1528882302', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317313954272', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('447', '10002', '20180613173153575610', '1.0000', '0.0050', '0.9950', '1528882313', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317313954272', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('448', '10002', '20180613173158101100', '1.0000', '0.0050', '0.9950', '1528882318', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317313954272', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('449', '10002', '20180613173232485210', '1.0000', '0.0050', '0.9950', '1528882352', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317323018146', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('450', '10002', '20180613173445535497', '1.0000', '0.0050', '0.9950', '1528882485', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317344352278', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('451', '10002', '20180613173516525353', '1.0000', '0.0050', '0.9950', '1528882516', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317351327196', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('452', '10002', '20180613173759551005', '1.0000', '0.0050', '0.9950', '1528882679', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317375898327', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('453', '10002', '20180613173832565010', '1.0000', '0.0050', '0.9950', '1528882712', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317383019884', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('454', '10002', '20180613173859515050', '1.0000', '0.0050', '0.9950', '1528882739', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061317385731741', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0000', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('455', '10002', '20180615172531981009', '0.0100', '0.0001', '0.0099', '1529054731', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517252869658', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('456', '10002', '20180615172552485648', '1.0000', '0.0080', '0.9920', '1529054752', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517254826108', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('457', '10002', '20180615172600565349', '10.0000', '0.0800', '9.9200', '1529054760', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517255587777', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('458', '10002', '20180615172617571005', '0.0100', '0.0001', '0.0099', '1529054777', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517261419577', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('459', '10002', '20180615172634975110', '0.0100', '0.0001', '0.0099', '1529054794', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517263210911', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('460', '10002', '20180615172653100529', '0.0100', '0.0001', '0.0099', '1529054813', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517263210911', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('461', '10002', '20180615172655102101', '0.0100', '0.0001', '0.0099', '1529054815', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517263210911', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('462', '10002', '20180615172700525052', '0.0100', '0.0001', '0.0099', '1529054820', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517265681424', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('463', '10002', '20180615172705579910', '1.0000', '0.0080', '0.9920', '1529054825', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517270238872', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('464', '10002', '20180615172713494854', '10.0000', '0.0800', '9.9200', '1529054833', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517270819882', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('465', '10002', '20180615172938505354', '1.0000', '0.0080', '0.9920', '1529054978', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517293457779', '0', '100204', '91EF44B3DC38C78B4DC7A27721587319', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '100204', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('466', '10002', '20180615173509100564', '0.0100', '0.0001', '0.0099', '1529055309', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517350788879', '0', '2', '893701E6A0C5EB903BFF6A12818ED9E0', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '测试账号', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('467', '10002', '20180615173516524949', '1.0000', '0.0080', '0.9920', '1529055316', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517350788879', '0', '2', '893701E6A0C5EB903BFF6A12818ED9E0', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '测试账号', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('468', '10002', '20180615173529495497', '10.0000', '0.0800', '9.9200', '1529055329', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517350788879', '0', '2', '893701E6A0C5EB903BFF6A12818ED9E0', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '测试账号', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('469', '10002', '20180615173534541015', '10.0000', '0.0800', '9.9200', '1529055334', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Htbank', '汇通网关支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061517350788879', '0', '2', '893701E6A0C5EB903BFF6A12818ED9E0', '', '0', '0', 'Htbank', '汇通网关支付', '0', '1234|456', '测试账号', '0.0000', '0.0000', '229', '213', '1');
INSERT INTO `pay_order` VALUES ('470', '10002', '20180615182557539997', '0.0100', '0.0001', '0.0099', '1529058357', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061518255436759', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('471', '10002', '20180615184443984957', '0.0100', '0.0001', '0.0099', '1529059483', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061518444093751', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('472', '10002', '20180615185604525410', '1.0000', '0.0080', '0.9920', '1529060164', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061518560149828', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('473', '10002', '20180615190355985398', '0.0100', '0.0001', '0.0099', '1529060635', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519035297962', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('474', '10002', '20180615190417499710', '2.0000', '0.0160', '1.9840', '1529060657', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519041481872', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('475', '10002', '20180615190500995498', '0.0100', '0.0001', '0.0099', '1529060700', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519045824397', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('476', '10002', '20180615190549100519', '2.0000', '0.0160', '1.9840', '1529060749', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519054624180', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('477', '10002', '20180615190617579748', '2.0000', '0.0160', '1.9840', '1529060777', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519061417141', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('478', '10002', '20180615190706979799', '2.0000', '0.0160', '1.9840', '1529060826', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519070119467', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('479', '10002', '20180615190956525048', '2.0000', '0.0160', '1.9840', '1529060996', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519095177873', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('480', '10002', '20180615191034975798', '0.0100', '0.0001', '0.0099', '1529061034', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519103279881', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('481', '10002', '20180615191041495651', '2.0000', '0.0160', '1.9840', '1529061041', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519103620621', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('482', '10002', '20180615191107984910', '0.0100', '0.0001', '0.0100', '1529061067', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519110345367', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0030', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('483', '10002', '20180615191121575350', '0.0100', '0.0001', '0.0100', '1529061081', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519111921958', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0030', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('484', '10002', '20180615191206545557', '0.0100', '0.0001', '0.0100', '1529061126', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519120450871', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0030', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('485', '10002', '20180615191227981011', '0.0100', '0.0001', '0.0100', '1529061148', '0', '902', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '微信扫码支付', '0', 'VIP基础服务', 'Rzfwxscan', '睿支付微信扫码', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519122658032', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfwxscan', '睿支付微信扫码', '0', '1234|456', '100000057', '0.0000', '0.0030', '228', '212', '1');
INSERT INTO `pay_order` VALUES ('486', '10002', '20180615191324525255', '0.0100', '0.0001', '0.0099', '1529061204', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519132214617', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('487', '10002', '20180615191335102545', '3.0000', '0.0240', '2.9760', '1529061215', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519133251698', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('488', '10002', '20180615191415559753', '0.0100', '0.0001', '0.0099', '1529061255', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519141348075', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('489', '10002', '20180615191434974848', '1.0000', '0.0080', '0.9920', '1529061274', '0', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '0', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519143098786', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');
INSERT INTO `pay_order` VALUES ('490', '10002', '20180615191459515352', '1.0000', '0.0080', '0.9920', '1529061299', '1529391569', '907', 'http://www.zuyapi.loc/demo/server.php', 'http://www.zuyapi.loc/demo/page.php', '网银支付', '2', 'VIP基础服务', 'Rzfbank', '睿支付网银支付', 'http://www.zuyapi.loc/demo/index1.php', 'E2018061519145633082', '0', '100000057', 'eS9xWlY1bkQ2ajFQV0FDT2VsbUhtZz09', '', '0', '0', 'Rzfbank', '睿支付网银支付', '0', '1234|456', '100000057', '0.0000', '0.0000', '230', '214', '1');

-- ----------------------------
-- Table structure for pay_paylog
-- ----------------------------
DROP TABLE IF EXISTS `pay_paylog`;
CREATE TABLE `pay_paylog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(50) NOT NULL,
  `result_code` varchar(50) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `fromuser` varchar(50) NOT NULL,
  `time_end` int(11) unsigned NOT NULL DEFAULT '0',
  `total_fee` smallint(6) unsigned NOT NULL DEFAULT '0',
  `payname` varchar(50) NOT NULL,
  `bank_type` varchar(20) DEFAULT NULL,
  `trade_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IND_TRD` (`transaction_id`),
  UNIQUE KEY `IND_ORD` (`out_trade_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_paylog
-- ----------------------------

-- ----------------------------
-- Table structure for pay_pay_channel_extend_fields
-- ----------------------------
DROP TABLE IF EXISTS `pay_pay_channel_extend_fields`;
CREATE TABLE `pay_pay_channel_extend_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '代付渠道ID',
  `code` varchar(64) NOT NULL DEFAULT '' COMMENT '代付渠道代码',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '扩展字段名',
  `alias` varchar(50) NOT NULL DEFAULT '' COMMENT '扩展字段别名',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `etime` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_pay_channel_extend_fields
-- ----------------------------

-- ----------------------------
-- Table structure for pay_pay_for_another
-- ----------------------------
DROP TABLE IF EXISTS `pay_pay_for_another`;
CREATE TABLE `pay_pay_for_another` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `code` varchar(64) NOT NULL COMMENT '代付代码',
  `title` varchar(64) NOT NULL COMMENT '代付名称',
  `mch_id` varchar(255) NOT NULL DEFAULT ' ' COMMENT '商户号',
  `appid` varchar(100) NOT NULL DEFAULT ' ' COMMENT '应用APPID',
  `appsecret` varchar(100) NOT NULL DEFAULT ' ' COMMENT '应用密钥',
  `signkey` varchar(500) NOT NULL DEFAULT ' ' COMMENT '加密的秘钥',
  `public_key` varchar(1000) NOT NULL DEFAULT '  ' COMMENT '加密的公钥',
  `private_key` varchar(1000) NOT NULL DEFAULT '  ' COMMENT '加密的私钥',
  `exec_gateway` varchar(255) NOT NULL DEFAULT ' ' COMMENT '请求代付的地址',
  `query_gateway` varchar(255) NOT NULL DEFAULT ' ' COMMENT '查询代付的地址',
  `serverreturn` varchar(255) NOT NULL DEFAULT ' ' COMMENT '服务器通知网址',
  `unlockdomain` varchar(255) NOT NULL DEFAULT ' ' COMMENT '防封域名',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更改时间',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1开启 0关闭',
  `is_default` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认：1是，0否',
  `cost_rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '成本费率',
  `rate_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '费率类型：按单笔收费0，按比例收费：1',
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `updatetime` (`updatetime`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='代付通道表';

-- ----------------------------
-- Records of pay_pay_for_another
-- ----------------------------
INSERT INTO `pay_pay_for_another` VALUES ('11', 'Ans', '爱农T0(承德向友)', '929080095023568', '', '', 'yVKWScFvy6a8UFKjC8BtR4p3UkDw4YHD', '', '', 'http://remit.chinagpay.com/bas/BgTrans', 'http://remit.chinagpay.com/bas/BgTrans', '', '', '1521195102', '0', '0', '0.0000', '0');
INSERT INTO `pay_pay_for_another` VALUES ('12', 'Dwj', '80001爱农', 'OLP800001000001a', '800001', '', 'SwJw1FoE8C1EGA1JSNFxVFpVv', '', '', 'http://103.47.137.51:8092/posm/mercdfreq.tran?olpdat=', 'http://103.47.137.51:8092/posm/mercdfqry.tran?olpdat=', '', '', '1528871539', '1', '1', '0.0000', '0');
INSERT INTO `pay_pay_for_another` VALUES ('13', 'AnsT1', '爱农普通(承德向友)', '929080095023568', '', '', 'yVKWScFvy6a8UFKjC8BtR4p3UkDw4YHD', '', '', 'http://remit.chinagpay.com/bas/BgTrans', 'http://remit.chinagpay.com/bas/BgTrans', '', '', '1521296459', '0', '0', '0.0000', '0');
INSERT INTO `pay_pay_for_another` VALUES ('14', 'Ips', '环迅代付', '207231', '2072310010', '北京朝莱商贸有限公司', 'Mo6HqQtlMv4j9hZwfRck7lVGSd8pG9C75ascLsjgVgw88Yv4v0kVEVpMmFcDESGMpyZuclb4p7HB2oAwXeAk9UYq0RsllfcwTAfpvROpC0jMLn9sJWlg9HMCxhhNpVrk', 'KTYAr4jV', 'x6dwjkThGdMj4TVh075lNwag', 'https://merservice.ips.com.cn/pfas-merws/services/issued?wsdl', 'https://merservice.ips.com.cn/pfas-merws/services/issued?wsdl', '', '', '1524043515', '0', '0', '0.0000', '0');
INSERT INTO `pay_pay_for_another` VALUES ('15', 'IpsTransfer', '易收付', '207231', 'KTYAr4jV', 'x6dwjkThGdMj4TVh075lNwag', 'Mo6HqQtlMv4j9hZwfRck7lVGSd8pG9C75ascLsjgVgw88Yv4v0kVEVpMmFcDESGMpyZuclb4p7HB2oAwXeAk9UYq0RsllfcwTAfpvROpC0jMLn9sJWlg9HMCxhhNpVrk', '2072310010', '', 'https://ebp.ips.com.cn/fpms-access/action/trade/transfer.do', '', '', '', '1524728654', '0', '0', '0.0000', '0');

-- ----------------------------
-- Table structure for pay_platform_report
-- ----------------------------
DROP TABLE IF EXISTS `pay_platform_report`;
CREATE TABLE `pay_platform_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initial_money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '期初余额',
  `income_money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '入金金额',
  `pay_wait_checked` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '出金待审核金额',
  `pay_success` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '出金成功金额',
  `pay_fail` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '出金失败金额',
  `income_profit` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '入金利润',
  `pay_profit` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '出金利润',
  `frozen_money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '冻结金额',
  `merchant_money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '商户实际到账金额',
  `platform_cost` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '平台成本',
  `platform_profit` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '平台利润',
  `end_profit` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '期末余额',
  `current_money` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '当前余额',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '生成时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_platform_report
-- ----------------------------

-- ----------------------------
-- Table structure for pay_product
-- ----------------------------
DROP TABLE IF EXISTS `pay_product`;
CREATE TABLE `pay_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '通道名称',
  `code` varchar(50) NOT NULL COMMENT '通道代码',
  `polling` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '接口模式 0 单独 1 轮询',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '支付类型 1 微信扫码 2 微信H5 3 支付宝扫码 4 支付宝H5 5 网银跳转 6网银直连  7 百度钱包  8 QQ钱包 9 京东钱包',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `isdisplay` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户端显示 1 显示 0 不显示',
  `channel` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '通道ID',
  `weight` text COMMENT '平台默认通道权重',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=911 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_product
-- ----------------------------
INSERT INTO `pay_product` VALUES ('901', '微信公众号', 'WXJSAPI', '0', '2', '1', '1', '0', '');
INSERT INTO `pay_product` VALUES ('902', '微信扫码支付', 'WXSCAN', '0', '1', '1', '1', '199', '');
INSERT INTO `pay_product` VALUES ('903', '支付宝扫码支付', 'ALISCAN', '0', '3', '1', '1', '201', '');
INSERT INTO `pay_product` VALUES ('904', '支付宝手机', 'ALIWAP', '0', '4', '1', '1', '202', '');
INSERT INTO `pay_product` VALUES ('905', 'QQ手机支付', 'QQWAP', '1', '2', '0', '1', '0', '200:7');
INSERT INTO `pay_product` VALUES ('907', '网银支付', 'DBANK', '0', '5', '1', '1', '205', '');
INSERT INTO `pay_product` VALUES ('908', 'QQ扫码支付', 'QSCAN', '0', '8', '0', '0', '203', '');
INSERT INTO `pay_product` VALUES ('909', '百度钱包', 'BAIDU', '0', '7', '0', '0', '0', '');
INSERT INTO `pay_product` VALUES ('910', '京东支付', 'JDPAY', '0', '9', '0', '0', '0', '');

-- ----------------------------
-- Table structure for pay_product_user
-- ----------------------------
DROP TABLE IF EXISTS `pay_product_user`;
CREATE TABLE `pay_product_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT ' ',
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户编号',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '商户通道ID',
  `polling` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '接口模式：0 单独 1 轮询',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '通道状态 0 关闭 1 启用',
  `channel` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '指定单独通道ID',
  `weight` varchar(255) DEFAULT NULL COMMENT '通道权重',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_product_user
-- ----------------------------
INSERT INTO `pay_product_user` VALUES ('14', '2', '907', '0', '1', '214', '209:|211:');
INSERT INTO `pay_product_user` VALUES ('15', '2', '901', '0', '1', '200', '');
INSERT INTO `pay_product_user` VALUES ('16', '2', '902', '0', '1', '212', '');
INSERT INTO `pay_product_user` VALUES ('17', '2', '903', '0', '1', '210', '');
INSERT INTO `pay_product_user` VALUES ('18', '2', '904', '0', '1', '208', '');
INSERT INTO `pay_product_user` VALUES ('19', '2', '905', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('20', '6', '901', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('21', '6', '902', '0', '1', '207', '');
INSERT INTO `pay_product_user` VALUES ('22', '6', '903', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('23', '6', '904', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('24', '6', '905', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('25', '6', '907', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('26', '180586943', '901', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('27', '180586943', '902', '0', '1', '199', '');
INSERT INTO `pay_product_user` VALUES ('28', '180586943', '903', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('29', '180586943', '904', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('30', '180586943', '905', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('31', '180586943', '907', '0', '1', '209', '');
INSERT INTO `pay_product_user` VALUES ('32', '3', '901', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('33', '3', '902', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('34', '3', '903', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('35', '3', '904', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('36', '3', '905', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('37', '3', '907', '0', '1', '209', '');
INSERT INTO `pay_product_user` VALUES ('38', '4', '901', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('39', '4', '902', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('40', '4', '903', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('41', '4', '904', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('42', '4', '905', '0', '0', '0', '');
INSERT INTO `pay_product_user` VALUES ('43', '4', '907', '0', '1', '209', '');

-- ----------------------------
-- Table structure for pay_reconciliation
-- ----------------------------
DROP TABLE IF EXISTS `pay_reconciliation`;
CREATE TABLE `pay_reconciliation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT '0' COMMENT '用户ID',
  `order_total_count` int(11) DEFAULT '0' COMMENT '总订单数',
  `order_success_count` int(11) DEFAULT '0' COMMENT '成功订单数',
  `order_fail_count` int(11) DEFAULT '0' COMMENT '未支付订单数',
  `order_total_amount` decimal(11,4) DEFAULT '0.0000' COMMENT '订单总额',
  `order_success_amount` decimal(11,4) DEFAULT '0.0000' COMMENT '订单实付总额',
  `date` date DEFAULT NULL COMMENT '日期',
  `ctime` int(11) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_reconciliation
-- ----------------------------
INSERT INTO `pay_reconciliation` VALUES ('1', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-03', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('2', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-02', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('3', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-01', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('4', '2', '10', '10', '10', '0.1000', '0.0000', '2018-05-31', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('5', '2', '1', '1', '1', '22.0000', '22.0000', '2018-05-30', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('6', '2', '0', '0', '0', '0.0000', '0.0000', '2018-05-29', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('7', '2', '0', '0', '0', '0.0000', '0.0000', '2018-05-28', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('8', '2', '0', '0', '0', '0.0000', '0.0000', '2018-05-27', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('9', '2', '0', '0', '0', '0.0000', '0.0000', '2018-05-26', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('10', '2', '0', '0', '0', '0.0000', '0.0000', '2018-05-25', '1528860656');
INSERT INTO `pay_reconciliation` VALUES ('11', '2', '0', '0', '0', null, null, '2018-06-13', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('12', '2', '0', '0', '0', null, null, '2018-06-12', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('13', '2', '0', '0', '0', null, null, '2018-06-11', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('14', '2', '0', '0', '0', null, null, '2018-06-10', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('15', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-09', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('16', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-08', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('17', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-07', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('18', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-06', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('19', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-05', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('20', '2', '0', '0', '0', '0.0000', '0.0000', '2018-06-04', '1528860667');
INSERT INTO `pay_reconciliation` VALUES ('21', '2', '0', '0', '0', '0.0000', '0.0000', '2017-08-27', '1528860681');
INSERT INTO `pay_reconciliation` VALUES ('22', '2', '0', '0', '0', null, null, '2018-05-24', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('23', '2', '0', '0', '0', null, null, '2018-05-23', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('24', '2', '0', '0', '0', null, null, '2018-05-22', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('25', '2', '0', '0', '0', null, null, '2018-05-21', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('26', '2', '0', '0', '0', null, null, '2018-05-20', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('27', '2', '0', '0', '0', null, null, '2018-05-19', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('28', '2', '0', '0', '0', null, null, '2018-05-18', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('29', '2', '0', '0', '0', null, null, '2018-05-17', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('30', '2', '0', '0', '0', null, null, '2018-05-16', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('31', '2', '0', '0', '0', null, null, '2018-05-15', '1528860819');
INSERT INTO `pay_reconciliation` VALUES ('32', '2', '0', '0', '0', null, null, '2018-06-19', '1529374873');
INSERT INTO `pay_reconciliation` VALUES ('33', '2', '0', '0', '0', null, null, '2018-06-18', '1529374873');
INSERT INTO `pay_reconciliation` VALUES ('34', '2', '0', '0', '0', null, null, '2018-06-17', '1529374873');
INSERT INTO `pay_reconciliation` VALUES ('35', '2', '0', '0', '0', null, null, '2018-06-16', '1529374873');
INSERT INTO `pay_reconciliation` VALUES ('36', '2', '36', '0', '36', '62.1800', null, '2018-06-15', '1529374873');
INSERT INTO `pay_reconciliation` VALUES ('37', '2', '0', '0', '0', null, null, '2018-06-14', '1529374873');

-- ----------------------------
-- Table structure for pay_redo_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_redo_order`;
CREATE TABLE `pay_redo_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '操作管理员',
  `money` decimal(11,4) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1：增加 2：减少',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '冲正备注',
  `date` datetime NOT NULL COMMENT '冲正周期',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_redo_order
-- ----------------------------
INSERT INTO `pay_redo_order` VALUES ('1', '2', '1', '1.0000', '1', '测试冲正', '2018-06-09 00:00:00', '1528516981');
INSERT INTO `pay_redo_order` VALUES ('2', '2', '1', '2.0000', '1', '测试【冲正周期\".2018-06-08】', '2018-06-08 00:00:00', '1528517142');
INSERT INTO `pay_redo_order` VALUES ('3', '2', '1', '1.0000', '1', '11【冲正周期:2018-06-11】', '2018-06-11 00:00:00', '1528708437');
INSERT INTO `pay_redo_order` VALUES ('4', '2', '1', '1.0000', '1', '1【冲正周期:2018-06-11】', '2018-06-11 00:00:00', '1528710434');
INSERT INTO `pay_redo_order` VALUES ('5', '4', '1', '100.0000', '1', '11【冲正周期:2018-06-19】', '2018-06-19 00:00:00', '1529380239');
INSERT INTO `pay_redo_order` VALUES ('6', '2', '1', '1111.0000', '1', '1【冲正周期:】', '0000-00-00 00:00:00', '1529395354');
INSERT INTO `pay_redo_order` VALUES ('7', '2', '1', '1.0000', '1', '1【冲正周期:】', '0000-00-00 00:00:00', '1529395571');
INSERT INTO `pay_redo_order` VALUES ('8', '2', '1', '1.0000', '1', '1【冲正周期:2018-06-19】', '2018-06-19 00:00:00', '1529395653');

-- ----------------------------
-- Table structure for pay_route
-- ----------------------------
DROP TABLE IF EXISTS `pay_route`;
CREATE TABLE `pay_route` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `urlstr` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_route
-- ----------------------------

-- ----------------------------
-- Table structure for pay_sms
-- ----------------------------
DROP TABLE IF EXISTS `pay_sms`;
CREATE TABLE `pay_sms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_key` varchar(255) DEFAULT NULL COMMENT 'App Key',
  `app_secret` varchar(255) DEFAULT NULL COMMENT 'App Secret',
  `sign_name` varchar(255) DEFAULT NULL COMMENT '默认签名',
  `is_open` int(11) DEFAULT '0' COMMENT '是否开启，0关闭，1开启',
  `admin_mobile` varchar(255) DEFAULT NULL COMMENT '管理员接收手机',
  `is_receive` int(11) DEFAULT '0' COMMENT '是否开启，0关闭，1开启',
  `sms_channel` varchar(20) NOT NULL DEFAULT 'aliyun' COMMENT '短信通道',
  `smsbao_user` varchar(50) NOT NULL DEFAULT '' COMMENT '短信宝账号',
  `smsbao_pass` varchar(50) NOT NULL DEFAULT '' COMMENT '短信宝密码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_sms
-- ----------------------------
INSERT INTO `pay_sms` VALUES ('3', 'LTAIRR5X8fMzp3do', 'Ku8NjuKhx9N6igyaHYstZFsc8GplwV', '德兴支付', '0', null, '0', 'aliyun', '1', '1');

-- ----------------------------
-- Table structure for pay_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `pay_sms_template`;
CREATE TABLE `pay_sms_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `template_code` varchar(255) DEFAULT NULL COMMENT '模板代码',
  `call_index` varchar(255) DEFAULT NULL COMMENT '调用字符串',
  `template_content` text COMMENT '模板内容',
  `ctime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_sms_template
-- ----------------------------
INSERT INTO `pay_sms_template` VALUES ('3', '修改支付密码', 'SMS_111795375', 'editPayPassword', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1512202260');
INSERT INTO `pay_sms_template` VALUES ('4', '修改登录密码', 'SMS_111795375', 'editPassword', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1512190115');
INSERT INTO `pay_sms_template` VALUES ('5', '异地登录', 'SMS_111795375', 'loginWarning', '您的账号于${time}登录异常，异常登录地址：${address}，如非本人操纵，请及时修改账号密码。', '1512202260');
INSERT INTO `pay_sms_template` VALUES ('6', '申请结算', 'SMS_111795375', 'clearing', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1512202260');
INSERT INTO `pay_sms_template` VALUES ('7', '委托结算', 'SMS_111795375', 'entrusted', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1512202260');
INSERT INTO `pay_sms_template` VALUES ('8', '绑定手机', 'SMS_119087905', 'bindMobile', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1514534290');
INSERT INTO `pay_sms_template` VALUES ('9', '更新手机', 'SMS_119087905', 'editMobile', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1514535688');
INSERT INTO `pay_sms_template` VALUES ('10', '更新银行卡 ', 'SMS_119087905 ', 'addBankcardSend', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1514535688');
INSERT INTO `pay_sms_template` VALUES ('11', '修改个人资料', 'SMS_119087905', 'saveProfile', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', null);
INSERT INTO `pay_sms_template` VALUES ('12', '绑定管理员手机号码', 'SMS_119087905', 'adminbindMobile', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');
INSERT INTO `pay_sms_template` VALUES ('13', '修改管理员手机号码', 'SMS_119087905', 'admineditMobile', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');
INSERT INTO `pay_sms_template` VALUES ('14', '批量删除订单', 'SMS_119087905', 'delOrderSend', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');
INSERT INTO `pay_sms_template` VALUES ('15', '解绑谷歌身份验证器', 'SMS_119087905', 'unbindGoogle', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');
INSERT INTO `pay_sms_template` VALUES ('16', '设置订单为已支付', 'SMS_119087905', 'setOrderPaidSend', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');
INSERT INTO `pay_sms_template` VALUES ('17', '清理数据', 'SMS_119087905', 'clearDataSend', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');
INSERT INTO `pay_sms_template` VALUES ('18', '增加/减少余额（冲正）', 'SMS_119087905', 'adjustUserMoneySend', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');
INSERT INTO `pay_sms_template` VALUES ('19', '提交代付', 'SMS_119087905', 'submitDfSend', '您的验证码为：${code} ，你正在进行${opration}操作，该验证码 5 分钟内有效，请勿泄露于他人。', '1527670734');

-- ----------------------------
-- Table structure for pay_systembank
-- ----------------------------
DROP TABLE IF EXISTS `pay_systembank`;
CREATE TABLE `pay_systembank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bankcode` varchar(100) DEFAULT NULL,
  `bankname` varchar(300) DEFAULT NULL,
  `images` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=198 DEFAULT CHARSET=utf8 COMMENT='结算银行';

-- ----------------------------
-- Records of pay_systembank
-- ----------------------------
INSERT INTO `pay_systembank` VALUES ('162', 'BOB', '北京银行', 'BOB.gif');
INSERT INTO `pay_systembank` VALUES ('164', 'BEA', '东亚银行', 'BEA.gif');
INSERT INTO `pay_systembank` VALUES ('165', 'ICBC', '中国工商银行', 'ICBC.gif');
INSERT INTO `pay_systembank` VALUES ('166', 'CEB', '中国光大银行', 'CEB.gif');
INSERT INTO `pay_systembank` VALUES ('167', 'GDB', '广发银行', 'GDB.gif');
INSERT INTO `pay_systembank` VALUES ('168', 'HXB', '华夏银行', 'HXB.gif');
INSERT INTO `pay_systembank` VALUES ('169', 'CCB', '中国建设银行', 'CCB.gif');
INSERT INTO `pay_systembank` VALUES ('170', 'BCM', '交通银行', 'BCM.gif');
INSERT INTO `pay_systembank` VALUES ('171', 'CMSB', '中国民生银行', 'CMSB.gif');
INSERT INTO `pay_systembank` VALUES ('172', 'NJCB', '南京银行', 'NJCB.gif');
INSERT INTO `pay_systembank` VALUES ('173', 'NBCB', '宁波银行', 'NBCB.gif');
INSERT INTO `pay_systembank` VALUES ('174', 'ABC', '中国农业银行', '5414c87492ad8.gif');
INSERT INTO `pay_systembank` VALUES ('175', 'PAB', '平安银行', '5414c0929a632.gif');
INSERT INTO `pay_systembank` VALUES ('176', 'BOS', '上海银行', 'BOS.gif');
INSERT INTO `pay_systembank` VALUES ('177', 'SPDB', '上海浦东发展银行', 'SPDB.gif');
INSERT INTO `pay_systembank` VALUES ('178', 'SDB', '深圳发展银行', 'SDB.gif');
INSERT INTO `pay_systembank` VALUES ('179', 'CIB', '兴业银行', 'CIB.gif');
INSERT INTO `pay_systembank` VALUES ('180', 'PSBC', '中国邮政储蓄银行', 'PSBC.gif');
INSERT INTO `pay_systembank` VALUES ('181', 'CMBC', '招商银行', 'CMBC.gif');
INSERT INTO `pay_systembank` VALUES ('182', 'CZB', '浙商银行', 'CZB.gif');
INSERT INTO `pay_systembank` VALUES ('183', 'BOC', '中国银行', 'BOC.gif');
INSERT INTO `pay_systembank` VALUES ('184', 'CNCB', '中信银行', 'CNCB.gif');
INSERT INTO `pay_systembank` VALUES ('193', 'ALIPAY', '支付宝', '58b83a5820644.jpg');
INSERT INTO `pay_systembank` VALUES ('194', 'WXZF', '微信支付', '58b83a757a298.jpg');

-- ----------------------------
-- Table structure for pay_template
-- ----------------------------
DROP TABLE IF EXISTS `pay_template`;
CREATE TABLE `pay_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT ' ' COMMENT '模板名称',
  `theme` varchar(255) NOT NULL DEFAULT ' ' COMMENT '模板代码',
  `is_default` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认模板:1是，0否',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `remarks` varchar(255) NOT NULL DEFAULT ' ' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='模板表';

-- ----------------------------
-- Records of pay_template
-- ----------------------------
INSERT INTO `pay_template` VALUES ('1', ' 默认模板', 'default', '0', '1524299660', '1524299660', ' 默认模板');
INSERT INTO `pay_template` VALUES ('2', '模板一', 'view1', '0', '1524299660', '1524299660', '模板一');
INSERT INTO `pay_template` VALUES ('3', '模板二', 'view2', '0', '0', '1524623473', '模板二');
INSERT INTO `pay_template` VALUES ('4', '模板三', 'view3', '1', '0', '1524623494', '模板三');

-- ----------------------------
-- Table structure for pay_tikuanconfig
-- ----------------------------
DROP TABLE IF EXISTS `pay_tikuanconfig`;
CREATE TABLE `pay_tikuanconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户编号',
  `tkzxmoney` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔最小提款金额',
  `tkzdmoney` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔最大提款金额',
  `dayzdmoney` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '当日提款最大总金额',
  `dayzdnum` int(11) NOT NULL DEFAULT '0' COMMENT '当日提款最大次数',
  `t1zt` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'T+1 ：1开启 0 关闭',
  `t0zt` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'T+0 ：1开启 0 关闭',
  `gmt0` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '购买T0',
  `tkzt` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '提款设置 1 开启 0 关闭',
  `tktype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '提款手续费类型 1 每笔 0 比例 ',
  `systemxz` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 系统规则 1 用户规则',
  `sxfrate` varchar(20) DEFAULT NULL COMMENT '单笔提款比例',
  `sxffixed` varchar(20) DEFAULT NULL COMMENT '单笔提款手续费',
  `issystem` tinyint(1) unsigned DEFAULT '0' COMMENT '平台规则 1 是 0 否',
  `allowstart` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '提款允许开始时间',
  `allowend` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '提款允许结束时间',
  `daycardzdmoney` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '单人单卡单日最高提现额',
  `auto_df_switch` tinyint(1) NOT NULL DEFAULT '0' COMMENT '自动代付开关',
  `auto_df_maxmoney` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '单笔代付最大金额限制',
  `auto_df_stime` varchar(20) NOT NULL DEFAULT '' COMMENT '自动代付开始时间',
  `auto_df_etime` varchar(20) NOT NULL DEFAULT '' COMMENT '自动代付结束时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IND_UID` (`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_tikuanconfig
-- ----------------------------
INSERT INTO `pay_tikuanconfig` VALUES ('28', '1', '100.00', '100000.00', '1000.00', '20', '0', '0', '200.00', '1', '1', '0', '2', '5', '1', '0', '0', '2000.00', '1', '10000.00', '00:00', '23:59');
INSERT INTO `pay_tikuanconfig` VALUES ('29', '2', '100.00', '10000.00', '100000.00', '20', '1', '0', '0.00', '1', '1', '1', '', '10', '0', '0', '0', '0.00', '0', '0.00', '', '');

-- ----------------------------
-- Table structure for pay_tikuanholiday
-- ----------------------------
DROP TABLE IF EXISTS `pay_tikuanholiday`;
CREATE TABLE `pay_tikuanholiday` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排除日期',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='排除节假日';

-- ----------------------------
-- Records of pay_tikuanholiday
-- ----------------------------
INSERT INTO `pay_tikuanholiday` VALUES ('5', '1503676800');
INSERT INTO `pay_tikuanholiday` VALUES ('6', '1503763200');
INSERT INTO `pay_tikuanholiday` VALUES ('8', '1504281600');
INSERT INTO `pay_tikuanholiday` VALUES ('9', '1504368000');

-- ----------------------------
-- Table structure for pay_tikuanmoney
-- ----------------------------
DROP TABLE IF EXISTS `pay_tikuanmoney`;
CREATE TABLE `pay_tikuanmoney` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '结算用户ID',
  `websiteid` int(11) NOT NULL DEFAULT '0',
  `payapiid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '结算通道ID',
  `t` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '结算方式: 1 T+1 ,0 T+0',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `datetype` varchar(2) NOT NULL,
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=691 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_tikuanmoney
-- ----------------------------

-- ----------------------------
-- Table structure for pay_tikuantime
-- ----------------------------
DROP TABLE IF EXISTS `pay_tikuantime`;
CREATE TABLE `pay_tikuantime` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `baiks` tinyint(2) unsigned DEFAULT '0' COMMENT '白天提款开始时间',
  `baijs` tinyint(2) unsigned DEFAULT '0' COMMENT '白天提款结束时间',
  `wanks` tinyint(2) unsigned DEFAULT '0' COMMENT '晚间提款开始时间',
  `wanjs` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='提款时间';

-- ----------------------------
-- Records of pay_tikuantime
-- ----------------------------
INSERT INTO `pay_tikuantime` VALUES ('1', '24', '17', '18', '24');

-- ----------------------------
-- Table structure for pay_tklist
-- ----------------------------
DROP TABLE IF EXISTS `pay_tklist`;
CREATE TABLE `pay_tklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `bankname` varchar(300) NOT NULL,
  `bankzhiname` varchar(300) NOT NULL,
  `banknumber` varchar(300) NOT NULL,
  `bankfullname` varchar(300) NOT NULL,
  `sheng` varchar(300) NOT NULL,
  `shi` varchar(300) NOT NULL,
  `sqdatetime` datetime DEFAULT NULL,
  `cldatetime` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `tkmoney` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `sxfmoney` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `money` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `t` int(4) NOT NULL DEFAULT '1',
  `payapiid` int(11) NOT NULL DEFAULT '0',
  `memo` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_tklist
-- ----------------------------
INSERT INTO `pay_tklist` VALUES ('1', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-05-30 10:11:21', '2018-06-15 10:10:30', '3', '100.0000', '5.0000', '95.0000', '0', '0', null);

-- ----------------------------
-- Table structure for pay_updatelog
-- ----------------------------
DROP TABLE IF EXISTS `pay_updatelog`;
CREATE TABLE `pay_updatelog` (
  `version` varchar(20) NOT NULL,
  `lastupdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_updatelog
-- ----------------------------

-- ----------------------------
-- Table structure for pay_userrate
-- ----------------------------
DROP TABLE IF EXISTS `pay_userrate`;
CREATE TABLE `pay_userrate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `payapiid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '通道ID',
  `feilv` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '运营费率',
  `fengding` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '封顶费率',
  `t0feilv` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0运营费率',
  `t0fengding` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'T0封顶费率',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='商户通道费率';

-- ----------------------------
-- Records of pay_userrate
-- ----------------------------
INSERT INTO `pay_userrate` VALUES ('21', '2', '907', '0.0080', '0.0150', '0.0080', '0.0150');
INSERT INTO `pay_userrate` VALUES ('22', '2', '901', '0.0050', '0.0080', '0.0050', '0.0080');
INSERT INTO `pay_userrate` VALUES ('23', '2', '902', '0.0050', '0.0080', '0.0050', '0.0080');
INSERT INTO `pay_userrate` VALUES ('24', '3', '901', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('25', '3', '902', '0.0300', '0.0000', '0.0300', '0.0000');
INSERT INTO `pay_userrate` VALUES ('26', '3', '903', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('27', '3', '904', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('28', '3', '907', '0.0050', '0.0050', '0.0050', '0.0050');
INSERT INTO `pay_userrate` VALUES ('29', '4', '901', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('30', '4', '902', '0.0600', '0.0000', '0.0600', '0.0000');
INSERT INTO `pay_userrate` VALUES ('31', '4', '903', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('32', '4', '904', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('33', '4', '907', '0.0060', '0.0060', '0.0060', '0.0060');
INSERT INTO `pay_userrate` VALUES ('34', '5', '901', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('35', '5', '902', '0.0900', '0.0000', '0.0900', '0.0000');
INSERT INTO `pay_userrate` VALUES ('36', '5', '903', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('37', '5', '904', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('38', '5', '907', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('39', '6', '901', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('40', '6', '902', '0.1000', '0.0000', '0.1000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('41', '6', '903', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('42', '6', '904', '0.0000', '0.0000', '0.0000', '0.0000');
INSERT INTO `pay_userrate` VALUES ('43', '6', '907', '0.0000', '0.0000', '0.0000', '0.0000');

-- ----------------------------
-- Table structure for pay_user_code
-- ----------------------------
DROP TABLE IF EXISTS `pay_user_code`;
CREATE TABLE `pay_user_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '0' COMMENT '0找回密码',
  `code` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ctime` int(11) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL COMMENT '更新时间',
  `endtime` int(11) DEFAULT NULL COMMENT '有效时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_user_code
-- ----------------------------

-- ----------------------------
-- Table structure for pay_user_riskcontrol_config
-- ----------------------------
DROP TABLE IF EXISTS `pay_user_riskcontrol_config`;
CREATE TABLE `pay_user_riskcontrol_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `min_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔最小金额',
  `max_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔最大金额',
  `unit_all_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单位时间内交易总金额',
  `all_money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '当天交易总金额',
  `start_time` tinyint(10) unsigned NOT NULL DEFAULT '0' COMMENT '一天交易开始时间',
  `end_time` tinyint(10) unsigned NOT NULL DEFAULT '0' COMMENT '一天交易结束时间',
  `unit_number` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间内交易的总笔数',
  `is_system` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否平台规则',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态:1开通，0关闭',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `edit_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `time_unit` char(1) NOT NULL DEFAULT 'i' COMMENT '限制的时间单位',
  `unit_interval` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '单位时间值',
  `domain` varchar(255) NOT NULL DEFAULT ' ' COMMENT '防封域名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='交易配置';

-- ----------------------------
-- Records of pay_user_riskcontrol_config
-- ----------------------------
INSERT INTO `pay_user_riskcontrol_config` VALUES ('1', '0', '0.00', '0.00', '0.00', '0.00', '0', '0', '0', '1', '0', '1522289886', '0', 'i', '0', '');

-- ----------------------------
-- Table structure for pay_version
-- ----------------------------
DROP TABLE IF EXISTS `pay_version`;
CREATE TABLE `pay_version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(255) NOT NULL DEFAULT '0' COMMENT '版本',
  `author` varchar(255) NOT NULL DEFAULT ' ' COMMENT '作者',
  `save_time` varchar(255) NOT NULL DEFAULT '0000-00-00' COMMENT '修改时间,格式YYYY-mm-dd',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='数据库版本表';

-- ----------------------------
-- Records of pay_version
-- ----------------------------
INSERT INTO `pay_version` VALUES ('1', '5.5', '陈嘉杰', '2018-4-8');
INSERT INTO `pay_version` VALUES ('2', '5.6', ' mapeijian', '2018/4/17 17:45:33');
INSERT INTO `pay_version` VALUES ('3', '5.7', ' mio', '2018-4-17');

-- ----------------------------
-- Table structure for pay_websiteconfig
-- ----------------------------
DROP TABLE IF EXISTS `pay_websiteconfig`;
CREATE TABLE `pay_websiteconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `websitename` varchar(300) DEFAULT NULL COMMENT '网站名称',
  `domain` varchar(300) DEFAULT NULL COMMENT '网址',
  `email` varchar(100) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `qq` varchar(30) DEFAULT NULL,
  `directory` varchar(100) DEFAULT NULL COMMENT '后台目录名称',
  `icp` varchar(100) DEFAULT NULL,
  `tongji` varchar(1000) DEFAULT NULL COMMENT '统计',
  `login` varchar(100) DEFAULT NULL COMMENT '登录地址',
  `payingservice` tinyint(1) unsigned DEFAULT '0' COMMENT '商户代付 1 开启 0 关闭',
  `authorized` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '商户认证 1 开启 0 关闭',
  `invitecode` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '邀请码注册',
  `company` varchar(200) DEFAULT NULL COMMENT '公司名称',
  `serverkey` varchar(50) DEFAULT NULL COMMENT '授权服务key',
  `withdraw` tinyint(1) DEFAULT '0' COMMENT '提现通知：0关闭，1开启',
  `login_warning_num` tinyint(3) unsigned NOT NULL DEFAULT '3' COMMENT '前台可以错误登录次数',
  `login_ip` varchar(1000) NOT NULL DEFAULT ' ' COMMENT '登录IP',
  `is_repeat_order` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否允许重复订单:1是，0否',
  `google_auth` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启谷歌身份验证登录',
  `df_api` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启代付API',
  `logo` varchar(255) NOT NULL DEFAULT ' ' COMMENT '公司logo',
  `random_mchno` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启随机商户号',
  `register_need_activate` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户注册是否需激活',
  `admin_alone_login` tinyint(1) NOT NULL DEFAULT '0' COMMENT '管理员是否只允许同时一次登录',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_websiteconfig
-- ----------------------------
INSERT INTO `pay_websiteconfig` VALUES ('1', 'api支付系统', 'www.zuyapi.loc', 'support@pay.com', '4001234456', '', 'manage', '沪ICP备12031756号', '', 'pay9', '1', '1', '1', '', '0d6de302cbc615de3b09463acea87662', '0', '3', ' ', '1', '0', '1', ' ', '0', '1', '0');

-- ----------------------------
-- Table structure for pay_wttklist
-- ----------------------------
DROP TABLE IF EXISTS `pay_wttklist`;
CREATE TABLE `pay_wttklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `bankname` varchar(300) NOT NULL,
  `bankzhiname` varchar(300) NOT NULL,
  `banknumber` varchar(300) NOT NULL,
  `bankfullname` varchar(300) NOT NULL,
  `sheng` varchar(300) NOT NULL,
  `shi` varchar(300) NOT NULL,
  `sqdatetime` datetime DEFAULT NULL,
  `cldatetime` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `tkmoney` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '提款金额',
  `sxfmoney` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '手续费',
  `money` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '实际到账',
  `t` int(4) NOT NULL DEFAULT '1',
  `payapiid` int(11) NOT NULL DEFAULT '0',
  `memo` text COMMENT '备注',
  `additional` varchar(1000) NOT NULL DEFAULT ' ' COMMENT '额外的参数',
  `code` varchar(64) NOT NULL DEFAULT ' ' COMMENT '代码控制器名称',
  `df_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '代付通道id',
  `df_name` varchar(64) NOT NULL DEFAULT ' ' COMMENT '代付名称',
  `orderid` varchar(100) NOT NULL DEFAULT ' ' COMMENT '订单id',
  `cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '成本',
  `cost_rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '成本费率',
  `rate_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '费率类型：按单笔收费0，按比例收费：1',
  `extends` text COMMENT '扩展数据',
  `out_trade_no` varchar(30) DEFAULT '' COMMENT '下游订单号',
  `df_api_id` int(11) DEFAULT '0' COMMENT '代付API申请ID',
  `auto_submit_try` int(10) NOT NULL DEFAULT '0' COMMENT '自动代付尝试提交次数',
  `is_auto` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否自动提交',
  `last_submit_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后提交时间',
  `df_lock` tinyint(1) NOT NULL DEFAULT '0' COMMENT '代付锁，防止重复提交',
  `auto_query_num` int(10) NOT NULL DEFAULT '0' COMMENT '自动查询次数',
  `channel_mch_id` varchar(50) NOT NULL DEFAULT '' COMMENT '通道商户号',
  `df_charge_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '代付API扣除手续费方式，0：从到账金额里扣，1：从商户余额里扣',
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `df_id` (`df_id`),
  KEY `orderid` (`orderid`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_wttklist
-- ----------------------------
INSERT INTO `pay_wttklist` VALUES ('20', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-05-30 11:03:46', null, '0', '100.0000', '5.0000', '95.0000', '0', '0', null, '', ' ', '0', ' ', 'H0530494268984682', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('21', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-05-30 11:27:50', null, '0', '100.0000', '5.0000', '95.0000', '0', '0', null, '', ' ', '0', ' ', 'H0530508708137860', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('24', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-05-30 11:38:37', null, '0', '101.0000', '5.0000', '96.0000', '0', '0', null, '', ' ', '0', ' ', 'H0530515178959059', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('25', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-05-30 11:38:37', null, '0', '102.0000', '5.0000', '97.0000', '0', '0', null, '', ' ', '0', ' ', 'H0530515178975627', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('26', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-05-30 11:38:37', null, '0', '103.0000', '5.0000', '98.0000', '0', '0', null, '', ' ', '0', ' ', 'H0530515178985969', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('27', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-05-30 11:38:37', null, '0', '104.0000', '5.0000', '99.0000', '0', '0', null, '', ' ', '0', ' ', 'H0530515178996043', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('33', '2', '112121', '12121', '212121', '2121', '2121', '212121', '2018-06-01 13:27:20', null, '4', '100.0000', '5.0000', '95.0000', '0', '0', '参数错误，长度不正确（结算卡）', '', 'Dwj', '12', '80001爱农', 'H0601308400200127', '0.00', '0.0000', '0', '{\"lhh\":\"121\"}', '201806011322112911', '22', '0', '0', '0', '0', '0', 'OLP800001000001', '0');
INSERT INTO `pay_wttklist` VALUES ('34', '2', '234234', '23423', '23423', '234234234', '234', '43223', '2018-06-01 17:24:27', '2018-06-11 12:06:42', '4', '100.0000', '5.0000', '95.0000', '0', '0', '参数错误，长度不正确（结算卡）', '', 'Dwj', '12', '80001爱农', 'H0601450678889614', '0.00', '0.0000', '0', '{\"lhh\":\"4234\"}', '201806011724274583', '23', '0', '0', '0', '0', '0', 'OLP800001000001a', '0');
INSERT INTO `pay_wttklist` VALUES ('35', '2', '中国工商银行', '广州石牌支行', '23423', '马培坚', '234', '43223', '2018-06-01 17:26:42', '2018-06-11 12:06:42', '3', '100.0000', '5.0000', '95.0000', '0', '0', '', '', ' ', '0', ' ', 'H0601452020606953', '0.00', '0.0000', '0', '{\"lhh\":\"4234\"}', '201806011726412997', '24', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('36', '2', '112121', '12121', '212121', '2121', '2121', '212121', '2018-06-01 17:28:14', '2018-06-12 17:56:11', '4', '100.0000', '5.0000', '95.0000', '0', '0', '参数错误，长度不正确（结算卡）', '', 'Dwj', '12', '80001爱农', 'H0601452942112740', '0.00', '0.0000', '0', '{\"lhh\":\"121\"}', '201806011301195354', '4', '0', '0', '0', '0', '0', 'OLP800001000001a', '0');
INSERT INTO `pay_wttklist` VALUES ('37', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-06-19 10:10:07', null, '0', '100.0000', '5.0000', '95.0000', '1', '0', null, '', ' ', '0', ' ', 'H0619742079993539', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('38', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-06-19 10:11:00', null, '0', '100.0000', '10.0000', '90.0000', '1', '0', null, '', ' ', '0', ' ', 'H0619742604038068', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('39', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-06-19 10:11:41', null, '0', '100.0000', '5.0000', '95.0000', '0', '0', null, '', ' ', '0', ' ', 'H0619743012378377', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('40', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-06-19 15:45:51', null, '0', '100.1000', '10.0000', '0.0000', '1', '0', null, '', ' ', '0', ' ', 'H0619943515169736', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('41', '2', '中国工商银行', '广州石牌支行', '6212263602033031881', '马培坚', '广东省', '广州市', '2018-06-19 15:48:15', null, '0', '100.1000', '10.0000', '90.1000', '1', '0', null, '', ' ', '0', ' ', 'H0619944954648462', '0.00', '0.0000', '0', '', '', '0', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('42', '2', '1', '1', '1', '1', '1', '1', '2018-06-19 18:52:17', '2018-06-19 18:57:08', '3', '100.0000', '10.0000', '100.0000', '1', '0', '', '', ' ', '0', ' ', 'H0619055371166854', '0.00', '0.0000', '0', '{\"lhh\":\"1\"}', '201806191852168827', '25', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `pay_wttklist` VALUES ('43', '2', '1', '1', '1', '1', '1', '1', '2018-06-19 19:00:45', '2018-06-19 19:01:51', '3', '100.0000', '10.0000', '100.0000', '1', '0', '', '', ' ', '0', ' ', 'H0619060459590870', '0.00', '0.0000', '0', '{\"lhh\":\"1\"}', '201806191900451071', '26', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `pay_wttklist` VALUES ('44', '2', '1', '1', '1', '1', '1', '1', '2018-06-19 19:06:12', '2018-06-19 19:07:35', '3', '100.0000', '10.0000', '100.0000', '1', '0', '', '', ' ', '0', ' ', 'H0619063721985868', '0.00', '0.0000', '0', '{\"lhh\":\"1\"}', '201806191906119790', '27', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `pay_wttklist` VALUES ('45', '2', '1', '1', '1', '1', '1', '1', '2018-06-19 19:08:18', '2018-06-19 19:08:40', '3', '100.0000', '10.0000', '100.0000', '1', '0', '', '', ' ', '0', ' ', 'H0619064980413981', '0.00', '0.0000', '0', '{\"lhh\":\"1\"}', '201806191908175000', '28', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `pay_wttklist` VALUES ('46', '2', '1', '1', '1', '1', '1', '1', '2018-06-19 19:12:15', '2018-06-19 19:12:35', '3', '100.0000', '10.0000', '90.0000', '1', '0', '', '', ' ', '0', ' ', 'H0619067357067854', '0.00', '0.0000', '0', '{\"lhh\":\"1\"}', '201806191912154786', '32', '0', '0', '0', '0', '0', '', '0');
INSERT INTO `pay_wttklist` VALUES ('47', '2', '1', '1', '1', '1', '1', '1', '2018-06-19 19:13:32', null, '0', '100.0000', '10.0000', '90.0000', '1', '0', null, '', ' ', '0', ' ', 'H0619068120771385', '0.00', '0.0000', '0', '{\"lhh\":\"1\"}', '201806191913317717', '33', '0', '0', '0', '0', '0', '', '0');
